#include "xgpio.h"
#include "xparameters.h"

#include "platform.h"
#include "xil_printf.h"

#include "sleep.h"

#define GPIO_OUTPUT (0x0)
#define GPIO_INPUT (0xF)

#define PORT_4_SWITCHES (1)
#define PORT_16_LED (2)

#define PORT_LCR_BUTTONS (1)


int state_before = 0;
int state_now = 0;

int main() {
  XGpio led, button;
  int switch_data = 0;
  int lcr_button_val = 0;
  int blink_val = 0;

  xil_printf("Init...");

  /** Initialization */

  XGpio_Initialize(&button,
                   XPAR_BOUTONS_DEVICE_ID);  // initialize button XGpio variable
  XGpio_Initialize(
      &led, XPAR_LED_SWITCH_DEVICE_ID);  // initialize button XGpio variable

  XGpio_SetDataDirection(
      &led, PORT_4_SWITCHES,
      GPIO_INPUT);  // set first channel tristate buffer to input
  XGpio_SetDataDirection(
      &led, PORT_16_LED,
      GPIO_OUTPUT);  // set first channel tristate buffer to output

  XGpio_SetDataDirection(
      &button, PORT_LCR_BUTTONS,
      GPIO_INPUT);  // set first channel tristate buffer to input

  int count = 0;
  init_platform();
  while (1) {
    switch_data = XGpio_DiscreteRead(&led, PORT_4_SWITCHES);  // get switch data
    lcr_button_val = XGpio_DiscreteRead(
        &button, PORT_LCR_BUTTONS);  // get buttons left, center and right data
    xil_printf("Data read from GPIO Input is  0x%x \n\r", (int)switch_data);
    if (switch_data == 0b00001) xil_printf("button 0 pressed\n\r");
    if (switch_data == 0b00010) xil_printf("button 1 pressed\n\r");
    if (switch_data == 0b00100) xil_printf("button 2 pressed\n\r");
    if (switch_data == 0b01000) xil_printf("button 3 pressed\n\r");

    if (switch_data & 0b00001) {  // if button 0 is pressed

      XGpio_DiscreteWrite(&led, PORT_16_LED,
                          blink_val);  // write switch data to the LEDs
      blink_val = !blink_val;
      usleep(200000);  // delay 200ms
    } else if (switch_data & 0b00010)  // if button 1 is pressed
    {
      state_now = lcr_button_val & 0b010;
      if (lcr_button_val & 0b001) {
        XGpio_DiscreteWrite(&led, PORT_16_LED,
                            0b0000000000001111);  // on the four leds in left
      } else if (state_now == 1 && ) {

        state_before = state_now;
        XGpio_DiscreteWrite(&led, PORT_16_LED,
                            0b0000000000000000);  // off the four leds in left
      } else if (lcr_button_val & 0b100) {
        count++;
        if (count >= 1600) count = 0;
        XGpio_DiscreteWrite(&led, PORT_16_LED, (count % 16));
      }
    }
    usleep(20000);  // delay 20ms
  }

  cleanup_platform();
  return 0;
}