#include "xgpio.h"
#include "xparameters.h"

#include "platform.h"
#include "xil_printf.h"

#define GPIO_OUTPUT (0x0)
#define GPIO_INPUT (0xF)

#define PORT_4_SWITCHES (1)
#define PORT_16_LED (2)

#define PORT_LCR_BUTTONS (1)

int main() {
  XGpio led, button;
  int switch_data = 0;

  xil_printf("Init...");

  /** Initialization */

  XGpio_Initialize(&button,
                   XPAR_BOUTONS_DEVICE_ID);  // initialize button XGpio variable
  XGpio_Initialize(
      &led, XPAR_LED_SWITCH_DEVICE_ID);  // initialize button XGpio variable

  XGpio_SetDataDirection(
      &led, PORT_4_SWITCHES,
      GPIO_INPUT);  // set first channel tristate buffer to input
  XGpio_SetDataDirection(
      &led, PORT_16_LED,
      GPIO_OUTPUT);  // set first channel tristate buffer to output

  XGpio_SetDataDirection(
      &button, PORT_LCR_BUTTONS,
      GPIO_INPUT);  // set first channel tristate buffer to input

  init_platform();
  while (1) {
    switch_data = XGpio_DiscreteRead(&led, PORT_4_SWITCHES);  // get switch data
    xil_printf("Data read from GPIO Input is  0x%x \n\r", (int)switch_data);

    if (switch_data == 0b00001) xil_printf("button 0 pressed\n\r");
    if (switch_data == 0b00010) xil_printf("button 1 pressed\n\r");
    if (switch_data == 0b00100) xil_printf("button 2 pressed\n\r");
    if (switch_data == 0b01000) xil_printf("button 3 pressed\n\r");

    XGpio_DiscreteWrite(&led, PORT_16_LED,
                        switch_data);  // write switch data to the LEDs
  }

  cleanup_platform();
  return 0;
}