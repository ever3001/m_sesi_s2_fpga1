#include "xgpio.h"
#include "xparameters.h"

#include "platform.h"
#include "xil_printf.h"

#define GPIO_OUTPUT (0x0)
#define GPIO_INPUT (0xF)

#define PORT_4_SWITCHES (1)
#define PORT_16_LED (2)

#define PORT_LCR_BUTTONS (1)

int main() {
  XGpio led, button;
  int switch_data = 0;

  xil_printf("Init...");

  /** Initialization */

  XGpio_Initialize(&button,
                   XPAR_BOUTONS_DEVICE_ID);  // initialize button XGpio variable
  XGpio_Initialize(
      &led, XPAR_LED_SWITCH_DEVICE_ID);  // initialize button XGpio variable

  XGpio_SetDataDirection(&led, 1,0xF);  // set first channel tristate buffer to input
  XGpio_SetDataDirection(&led, 2,0x0);  // set first channel tristate buffer to output

  XGpio_SetDataDirection(&button, 1,0xF);  // set first channel tristate buffer to input

  while (1) {
    switch_data = XGpio_DiscreteRead(&led, 1);  // get switch data


    XGpio_DiscreteWrite(&led, 2,switch_data);  // write switch data to the LEDs
  }

  return 0;
}