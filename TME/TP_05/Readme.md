# FPGA1 - PROJET: Centrale DCC sur FPGA

**Auteurs :**  

- Ever ATILANO  
- Joséphine MASINI  
- Apolline JOURET

Pour plus d'information par rapport au protocole DCC, regarder l'annoncé.
**Annoncé :** [FPGA1_TP5](./FPGA1_TP5_2020_Vivado.pdf)

## 1. SCHÉMA

![Architecture Generale](./Draws/Architecture%20de%20la%20centrale%20DCC.svg "Architecture Generale")

## 2. Compiler

Il faut avoir les logiciels [ghdl](http://ghdl.free.fr/) et [gtkwave](http://gtkwave.sourceforge.net/) pour pouvoir compiler et simuler le projet.  

Après d'installer les logiciels il faut tapper sur la terminal:

```bash
make all
```

## 3. CENTRALE DCC

- Source : [DCC_Central](./VHDL/src_DCC_Central.vhd)
- Test Bench : [Test Bench DCC_Central](./VHDL/tb_DCC_Central.vhd)

Ce module permet d'envoyer un signal DCC en sélectionnant un numéro de train et une commande.  

### CENTRALE DCC ENTITY

|       Nom       |              Description              | IN/OUT | NB_Bits |
|-----------------|---------------------------------------|:------:|:-------:|
| Clk_100MHz      | Entrée d'horloge 100 MHz              |   IN   |    1    |
| DCC_SIGNAL_OUT  | Signal de sortie DCC                  |   OUT  |    1    |
| LEDS_CMD        | Leds pour afficher la commande        |   OUT  |    4    |
| LEDS_ADDR_TRAIN | Leds pour afficher l'adresse du train |   OUT  |    3    |
| BTN_LEFT        | Bouton de gauche                      |   IN   |    1    |
| BTN_CENTER      | Bouton central                        |   IN   |    1    |
| BTN_RIGHT       | Bouton de droite                      |   IN   |    1    |
| SW_ADDR_TRAIN   | Sélectionner le numéro du train       |   IN   |    4    |
| OUT_END_REG     | Indique la fin d'envoi d'une trame    |   OUT  |    1    |
| Reset           | Reset asynchrone                      |   IN   |    1    |

### CENTRALE DCC SIMULATION

Envoi de commande Vitesse plus rapide (Step 28) :  

![Sim Vitesse plus rapide (Step 28)](./Draws/DCC_CENTRAL_CMD_02.png "Sim Vitesse plus rapide (Step 28)")

Envoi de commande Arrêter train (Stop) :  

![Sim Arrêter train (Stop)](./Draws/DCC_CENTRAL_CMD_03.png "Sim Arrêter train (Stop)")

### 3.1 DIVISEUR HORLOGE

- Source : [ClockDiv](./VHDL/src_ClockDiv.vhd)
- Test Bench : [Test Bench ClockDiv](./VHDL/tb_ClockDiv.vhd)

Ce module permet d'obtenir un signal périodique de fréquence F2, à partir d'un signal périodique de fréquence F1, la fréquence F2 étant toujours inférieure à la fréquence F1. Pour ce cas là on a une fréquence d'entrée de **100 MHz** avec un diviseur de fréquence de 100, donc à la sortie on aura une fréquence de **1 MHz**.  

La fréquence de 1 MHz va être utilisé dans les modules [TEMPO](#32-tempo) , [DCC_BIT_0](#33-dcc_bit_0) , [DCC_BIT_1](#34-dcc_bit_1)  

#### DIVISEUR HORLOGE ENTITY

|    Nom   |        Description       | IN/OUT | NB_Bits |
|----------|--------------------------|:------:|:-------:|
| ClockIn  | Entrée d'horloge 100 MHz |   IN   |    1    |
| ClockOut | Sortie d'horloge 1 MHz   |   OUT  |    1    |
| Reset    | Reset asynchrone         |   IN   |    1    |

#### DIVISEUR HORLOGE SIMULATION

Diviser la fréquence de 100 MHz a 1 Mhz :  

![Sim ClockDiv](./Draws/CLOCK_DIV_TEST.png "Sim ClockDiv")

### 3.2 TEMPO

- Source : [Tempo](./VHDL/src_Tempo.vhd)
- Test Bench : [Test Bench Tempo](./VHDL/tb_Tempo.vhd)

Ce module est un compteur de ms. On peut configurer le temps a compter (1 - 255 ms)  

#### TEMPO ENTITY

| Nom         | Description                                  | IN/OUT | NB_Bits |
|-------------|----------------------------------------------|:------:|:-------:|
| Clk_1MHz    | Entrée d'horloge 1 MHz                       |   IN   |    1    |
| StartFlag   | Commence à compter                           |   IN   |    1    |
| FinishFlag  | Signal de fin                                |   OUT  |    1    |
| TimeValInMs | Valeur du compteur interne en micro secondes |   IN   |    8    |
| Reset       | Reset asynchrone                             |   IN   |    1    |

#### TEMPO SIMULATION

Compter jusqu'à 6 ms :  

![Sim Tempo](./Draws/TEMPO_TEST.png "Sim Tempo")

### 3.3 DCC_BIT_0

- Source : [DCC_BIT_0](./VHDL/src_DCC_BIT.vhd)
- Test Bench : [Test Bench DCC_BIT_0](./VHDL/tb_DCC_BIT.vhd)

Ce module permet de générer un bit 0 au format DCC (regarder page 2 [FPGA1_TP5](./FPGA1_TP5_2020_Vivado.pdf) )  

On trouvera en sortie de ce module une porte OU qui joint les signaux de sortie de les modules DCC_BIT_0 et DCC_BIT_1.

#### DCC_BIT_0 ENTITY

| Nom              | Description              | IN/OUT | NB_Bits |
|------------------|--------------------------|:------:|:-------:|
| Clk_100MHz       | Entrée d'horloge 100 MHz |   IN   |    1    |
| Clk_1MHz         | Entrée d'horloge 1 MHz   |   IN   |    1    |
| BIT_0_GoFlag     | Signal de départ         |   IN   |    1    |
| BIT_0_FinishFlag | Signal de fin            |   OUT  |    1    |
| BIT_0_DccOut     | Signal de sortie         |   OUT  |    1    |
| Reset            | Reset asynchrone         |   IN   |    1    |

#### DCC_BIT_0 SIMULATION

Sortie d'un 0 dans le protocole DCC :  

![Sim DCC_BIT_0](./Draws/DCC_BIT_0_TEST.png "Sim DCC_BIT_0")

### 3.4 DCC_BIT_1

- Source : [DCC_BIT_1](./VHDL/src_DCC_BIT.vhd)
- Test Bench : [Test Bench DCC_BIT_1](./VHDL/tb_DCC_BIT.vhd)

Ce module permet de générer un bit 1 au format DCC (regarder page 2 [FPGA1_TP5](./FPGA1_TP5_2020_Vivado.pdf) )  

On trouvera en sortie de ce module une porte OU qui joint les signaux de sortie de les modules DCC_BIT_0 et DCC_BIT_1.

#### DCC_BIT_1 ENTITY

| Nom              | Description              | IN/OUT | NB_Bits |
|------------------|--------------------------|:------:|:-------:|
| Clk_100MHz       | Entrée d'horloge 100 MHz |   IN   |    1    |
| Clk_1MHz         | Entrée d'horloge 1 MHz   |   IN   |    1    |
| BIT_1_GoFlag     | Signal de départ         |   IN   |    1    |
| BIT_1_FinishFlag | Signal de fin            |   OUT  |    1    |
| BIT_1_DccOut     | Signal de sortie         |   OUT  |    1    |
| Reset            | Reset asynchrone         |   IN   |    1    |

#### DCC_BIT_1 SIMULATION

Sortie d'un 1 dans le protocole DCC :  

![Sim DCC_BIT_1](./Draws/DCC_BIT_1_TEST.png "Sim DCC_BIT_1")

### 3.5 REGISTRE DCC

- Source : [RegistreDCC](./VHDL/src_RegistreDCC.vhd)
- Test Bench : [Test Bench RegistreDCC](./VHDL/tb_RegistreDCC.vhd)

Ce module permet de sauvegarder une trame DCC et la transmettre au module de la [machine à états](#36-machine-%C3%A0-%C3%A9tats-mae)  

#### REGISTRE DCC ENTITY

| Nom         | Description                                                                 | IN/OUT |      NB_Bits     |
|-------------|-----------------------------------------------------------------------------|:------:|:----------------:|
| Clk_100MHz  | Entrée d'horloge 100 MHz                                                    |   IN   |         1        |
| DCC_In      | Commande DCC d'entrée                                                       |   IN   | (42, **51**, 60) |
| DCC_bit_Out | Commande DCC de sortie. Sort qu'un bit à la fois                            |   OUT  |         1        |
| END_REG     | Flag pour indiquer si le registre a finit de envoyer la trame ou pas        |   OUT  |         1        |
| COM_REG     | Commande le multiplexeur qui gère le registre, on a ici 4 possibilités ou 3 |   IN   |         1        |
| Reset       | Reset asynchrone                                                            |   IN   |         1        |

#### REGISTRE DCC SIMULATION

Sauvegarder et envoyer une commande DCC :  

![Sim RegistreDcc](./Draws/REGISTRE_DCC_TEST.png "Sim RegistreDcc")

### 3.6 MACHINE À ÉTATS (MAE)

- Source : [MAE](./VHDL/src_MAE.vhd)  
- Test Bench : [Test Bench MAE](./VHDL/tb_MAE.vhd)  

Ce module réalise la commande des autres blocs du système.  

#### MAE SCHÉMA

![MAE SCHEMA](./Draws/MAE_MODULE.svg "MAE SCHEMA")

### MAE ENTITY

| Nom                  | Description                                                                            | IN/OUT | NB_Bits |
|----------------------|----------------------------------------------------------------------------------------|:------:|:-------:|
| Clk_100MHz           | Entrée d'horloge 100 MHz                                                               |   IN   |    1    |
| REG_END_REG          | REGISTRE : Flag pour indiquer si le registre a finit de envoyer la trame ou pas        |   IN   |    1    |
| REG_COM_REG          | REGISTRE : Commande le multiplexeur qui gère le registre, on a ici 4 possibilités ou 3 |   OUT  |    2    |
| REG_DCC_BIT_OUT      | REGISTRE : Commande DCC d'entree à envoyer. Sort qu'un bit à la fois                   |   IN   |    1    |
| TMPO_StartFlag       | TEMPO : Commence à compter                                                             |   OUT  |    1    |
| TMPO_FinishFlag      | TEMPO : Signal de fin                                                                  |   IN   |    1    |
| TMPO_TimeValInMs     | TEMPO : Valeur du compteur interne en micro secondes                                   |   OUT  |    8    |
| DCC_BIT_0_GoFlag     | DCC_BIT_0 : Signal de départ                                                           |   OUT  |    1    |
| DCC_BIT_0_FinishFlag | DCC_BIT_0 : Signal de fin                                                              |   IN   |    1    |
| DCC_BIT_1_GoFlag     | DCC_BIT_1 : Signal de départ                                                           |   OUT  |    1    |
| DCC_BIT_1_FinishFlag | DCC_BIT_1 : Signal de fin                                                              |   IN   |    1    |
| Reset                | Reset asynchrone                                                                       |   IN   |    1    |

### MAE SIMULATION

Test les états de la machiné à états :  

![Sim MAE](./Draws/MAE_TEST.png "Sim MAE")

### 3.7 GÉNÉRATEUR TRAMES DE TEST

- Source : [Trame_DCC](./VHDL/src_Trame_DCC.vhd)  
- Test Bench : [Test Bench Trame_DCC](./VHDL/tb_Trame_DCC.vhd)  

Ce module sert à valider le reste de l'architecture. Il génére des trames DCC de test qui vont permettre de vérifier le bon fonctionnement du système.

#### COMMANDES À TESTER

1. Vitesse Lente (Step 5)
2. Vitesse plus rapide (Step 28)
3. Arrêter train (Stop)
4. Allumer les phares
5. Éteindre les phares
6. Activer klaxon
7. Désactiver klaxon
8. Activer Annonce entrée en gare
9. Désactiver Annonce entrée en gare

![GTT SCHEMA](./Draws/Générateur%20trames%20de%20test%20diagramme.png "GTT SCHEMA")

### GÉNÉRATEUR TRAMES DE TEST ENTITY

| Nom           | Description                   | IN/OUT |      NB_Bits     |
|---------------|-------------------------------|:------:|:----------------:|
| Clk_100       | Entrée d'horloge 100 MHz      |   IN   |         1        |
| Trame_DCC_OUT | Trame DCC pour le registre    |   OUT  | (42, **51**, 60) |
| SWITCHS       | Valeur de l'addresse du train |   IN   |         3        |
| Bouton_L      | Commande précedente           |   IN   |         1        |
| Bouton_R      | Commande suivante             |   IN   |         1        |
| Bouton_C      | Envoyer commande              |   IN   |         1        |
| Led_addr      | Afficher l'addresse du train  |   OUT  |         3        |
| Led_cmd       | Afficher le commande choisit  |   OUT  |         4        |
| Reset         | Reset asynchrone              |   IN   |         1        |

### GÉNÉRATEUR TRAMES DE TEST SIMULATION

Test la géneration du commande 2 :  

![Sim GTT](./Draws/GTT_TEST.png "Sim GTT")