#include "DCC.h"
#include <stdio.h>

#define F00_TO_F04_PREAMBULE (0b100)
#define F05_TO_F12_PREAMBULE (0b101)
#define F13_TO_F20_PREAMBULE (0b11011110)
#define FUNCTION_ERROR_VAL (0xFFFF)

#define TRAME_PREAMBULE (INT64_C(0b11111111111111))
#define TRAME_START_BIT (INT64_C(0b0))
#define TRAME_STOP_BIT (INT64_C(0b1))

const char trainSpeedArr[TRAIN_SPEED_END][32] = {
  "TRAIN_SPEED_STOP",    "TRAIN_SPEED_E_STOP",  "TRAIN_SPEED_STEP_01",
  "TRAIN_SPEED_STEP_03", "TRAIN_SPEED_STEP_05", "TRAIN_SPEED_STEP_07",
  "TRAIN_SPEED_STEP_09", "TRAIN_SPEED_STEP_11", "TRAIN_SPEED_STEP_13",
  "TRAIN_SPEED_STEP_15", "TRAIN_SPEED_STEP_17", "TRAIN_SPEED_STEP_19",
  "TRAIN_SPEED_STEP_21", "TRAIN_SPEED_STEP_23", "TRAIN_SPEED_STEP_25",
  "TRAIN_SPEED_STEP_27", "TRAIN_SPEED_STOP_I",  "TRAIN_SPEED_E_STOP_I",
  "TRAIN_SPEED_STEP_02", "TRAIN_SPEED_STEP_04", "TRAIN_SPEED_STEP_06",
  "TRAIN_SPEED_STEP_08", "TRAIN_SPEED_STEP_10", "TRAIN_SPEED_STEP_12",
  "TRAIN_SPEED_STEP_14", "TRAIN_SPEED_STEP_16", "TRAIN_SPEED_STEP_18",
  "TRAIN_SPEED_STEP_20", "TRAIN_SPEED_STEP_22", "TRAIN_SPEED_STEP_24",
  "TRAIN_SPEED_STEP_26", "TRAIN_SPEED_STEP_28",
};

const char trainFunctionsArr[TRAIN_FUNCTION_END][64] = {
  "TRAIN_FUNCTION_F00_LUMIERE_ON_OFF",
  "TRAIN_FUNCTION_F01_SON_ON_OFF",
  "TRAIN_FUNCTION_F02_COR_FRANCAISE_1",
  "TRAIN_FUNCTION_F03_COR_FRANCAISE_2",
  "TRAIN_FUNCTION_F04_TURBO_OFF",
  "TRAIN_FUNCTION_F05_COMPRESSEUR",
  "TRAIN_FUNCTION_F06_ACC_FRTMP_BCHTMDE_VITMAN",
  "TRAIN_FUNCTION_F07_COURBE_GRINCEMENT",
  "TRAIN_FUNCTION_F08_FERROVIARE_CLANK",
  "TRAIN_FUNCTION_F09_VENTILATEUR",
  "TRAIN_FUNCTION_F10_CONDUCTEUR_SIGNAL",
  "TRAIN_FUNCTION_F11_COURT_COR_FRANCAISE_1",
  "TRAIN_FUNCTION_F12_COURT_COR_FRANCAISE_2",
  "TRAIN_FUNCTION_F13_ANNONCE_STATION_FRANCAISE_1",
  "TRAIN_FUNCTION_F14_ANNONCE_STATION_FRANCAISE_2",
  "TRAIN_FUNCTION_F15_SIGNAL_ALERTE_FRANCAISE_1",
  "TRAIN_FUNCTION_F16_SIGNAL_ALERTE_FRANCAISE_2",
  "TRAIN_FUNCTION_F17_PORTE_CHAUFFEUR_OUVRIR_FERMER",
  "TRAIN_FUNCTION_F18_VALVE",
  "TRAIN_FUNCTION_F19_ATTELAGE",
  "TRAIN_FUNCTION_F20_SABLE",
  "TRAIN_FUNCTION_F21_LIBERATION_DES_FREINS",
};

uint16_t
getTrainFunctionCmd(t_trainFunctions fnc, bool flag)
{
  uint16_t cmd = 0;
  /**** Fonctions F0 à F4 */
  /**
   * 100F0 F4F3F2F1
   */
  if (fnc >= TRAIN_FUNCTION_F00_LUMIERE_ON_OFF &&
      fnc <= TRAIN_FUNCTION_F04_TURBO_OFF) {
    cmd |= (F00_TO_F04_PREAMBULE << 5);
    if (fnc == TRAIN_FUNCTION_F00_LUMIERE_ON_OFF) {
      cmd |= (flag << 4);
    } else {
      cmd |= (flag << (fnc - TRAIN_FUNCTION_F01_SON_ON_OFF));
    }
  }
  /**** Fonctions F5 à F12 */
  /**
   * 101S XXXX
   * 1011 F8F7F6F5
   * 1010 F12F11F10F9
   */
  else if (fnc >= TRAIN_FUNCTION_F05_COMPRESSEUR &&
           fnc <= TRAIN_FUNCTION_F12_COURT_COR_FRANCAISE_2) {
    cmd |= (F05_TO_F12_PREAMBULE << 5);
    if (fnc <= TRAIN_FUNCTION_F08_FERROVIARE_CLANK) {
      // S = 1
      cmd |= 1 << 4;
      cmd |= (flag << (fnc - TRAIN_FUNCTION_F05_COMPRESSEUR));
    } else {
      // S = 0
      cmd |= 0 << 4;
      cmd |= (flag << (fnc - TRAIN_FUNCTION_F09_VENTILATEUR));
    }
  }
  /**** Fonctions F13 à F20 */
  /**
   * 110 11110 XXXXXXXX
   * 110 11110 F20F19F18F17F16F15F14F13
   */
  else if (fnc >= TRAIN_FUNCTION_F13_ANNONCE_STATION_FRANCAISE_1 &&
           fnc <= TRAIN_FUNCTION_F20_SABLE) {
    cmd |= (F13_TO_F20_PREAMBULE << 8);
    cmd |= (flag << (fnc - TRAIN_FUNCTION_F13_ANNONCE_STATION_FRANCAISE_1));
  } else if (fnc == TRAIN_FUNCTION_F21_LIBERATION_DES_FREINS) {
    // TODO: Demander comment construire la fonction 21
    cmd = FUNCTION_ERROR_VAL - 1;
  } else {
    cmd = FUNCTION_ERROR_VAL;
  }
  return cmd;
}

/**
 * @brief Creer le commande pour la vitesse d'un train
 *
 * @note 01DXXXXX
 * @param spd - vitesse de la locomotive
 * @param dir - direction du train: 0 pour une marche arrière, 1 pour une
 * marche avant
 * @return cmd -
 */
uint8_t
getTrainSpeedCmd(t_trainSpeed spd, t_trainDirection dir)
{
  return ((0b01 << 6) | (dir << 5) | (spd));
}

uint64_t
createTrainSpeedDCCCmd(uint8_t bytesNo,
                       uint8_t trainNo,
                       t_trainSpeed spd,
                       t_trainDirection dir)
{
  uint64_t DccBuff = 0;
  uint8_t delta = 9 * bytesNo;
  uint8_t ctrlByte = trainNo;
  uint8_t bytesToSend[bytesNo];

  if (bytesNo > 3 || bytesNo < 1) {
    return UINT64_MAX;
  } else if (trainNo < 1 || trainNo > 6) {
    return UINT64_MAX;
  }

  DccBuff |= (TRAME_PREAMBULE << (19 + delta));  // PREAMBULE
  DccBuff |= (TRAME_START_BIT << (18 + delta));  // START BIT
  DccBuff |= ((int64_t)trainNo << (10 + delta)); // ADDR
  DccBuff |= (TRAME_START_BIT << (9 + delta));   // START BIT

  for (uint8_t i = 0; i < bytesNo; ++i) {
    bytesToSend[i] = getTrainSpeedCmd(spd, dir);
    DccBuff |=
      ((int64_t)bytesToSend[i] << (10 + 9 * (bytesNo - 1 - i))); // N BYTE
    // TODO: START BIT
    ctrlByte ^= bytesToSend[i];
  }
  DccBuff |= (int64_t)ctrlByte << 1; // CTRL BYTE*/

  DccBuff |= TRAME_STOP_BIT; // STOP BIT

  return DccBuff;
}

uint64_t
createFunctionDCCCmd(uint8_t trainNo, t_trainFunctions fnc, bool flag)
{
  uint64_t DccBuff = 0;
  uint8_t delta = 9 * 2;
  uint8_t ctrlByte = trainNo;
  uint16_t fncByte = getTrainFunctionCmd(fnc, flag);

  if (trainNo < 1 || trainNo > 6) {
    return UINT64_MAX;
  }

  DccBuff |= (TRAME_PREAMBULE << (19 + delta));  // PREAMBULE
  DccBuff |= (TRAME_START_BIT << (18 + delta));  // START BIT
  DccBuff |= ((int64_t)trainNo << (10 + delta)); // ADDR
  DccBuff |= (TRAME_START_BIT << (9 + delta));   // START BIT

  if (fnc < TRAIN_FUNCTION_F13_ANNONCE_STATION_FRANCAISE_1) {
    uint8_t byte1 = fncByte & (0b11111111);
    DccBuff |= byte1 << 19;
    ctrlByte ^= byte1;
    DccBuff |= byte1 << 10;
    ctrlByte ^= byte1;
  } else {
    uint8_t byte1 = fncByte >> 8 & (0b11111111);
    uint8_t byte2 = fncByte & (0b11111111);
    DccBuff |= byte1 << 19;
    ctrlByte ^= byte1;
    DccBuff |= byte2 << 10;
    ctrlByte ^= byte2;
  }

  DccBuff |= (int64_t)ctrlByte << 1; // CTRL BYTE*/

  DccBuff |= TRAME_STOP_BIT; // STOP BIT

  return DccBuff;
}

// C Program for the binary
// representation of a given number
void
printBinVal(uint64_t n)
{
  /* step 1 */
  if (n > 1)
    printBinVal(n / 2);

  /* step 2 */
  printf("%ld", n % 2);
}