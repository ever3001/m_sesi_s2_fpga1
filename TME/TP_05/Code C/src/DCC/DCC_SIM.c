#include "DCC.h"
#include <stdio.h>

int
main(int argc, char const* argv[])
{
  int inputUser = 0;
  printf("Select one function to create (1-9) :\n");
  printf("\t1. Vitesse Lente (Step 5)\n");
  printf("\t2. Vitesse plus rapide (Step 28)\n");
  printf("\t3. Arreter train (Stop)\n");
  printf("\t4. Allumer les phares\n");
  printf("\t5. Eteindre les phares\n");
  printf("\t6. Activer klaxon\n");
  printf("\t7. Desactiver klaxon\n");
  printf("\t8. Activer Annonce entree en gare\n");
  printf("\t9. Desactiver Annonce entree en gare\n");

  scanf("%d", &inputUser);

  printf("User selection = %d\n", inputUser);

  printf("Command to send: ");

  switch (inputUser) {

    case 1: {
      printf("111111111111110000001000011001000011001000000001001");
      break;
    }
    case 2: {
      printf("111111111111110000001000011111110011111110000001001");
      break;
    }
    case 3: {
      printf("111111111111110000001000011000000011000000000001001");
      break;
    }
    case 4: {
      printf("111111111111110000001000100100000100100000000001001");
      break;
    }
    case 5: {
      printf("111111111111110000001000100000000100000000000001001");
      break;
    }
    case 6: {
      printf("111111111111110000001000101001000101001000000001001");
      break;
    }
    case 7: {
      printf("111111111111110000001000101000000101000000000001001");
      break;
    }
    case 8: {
      printf("111111111111110000001000110111100000000010110110111");
      break;
    }
    case 9: {
      printf("111111111111110000001000110111100000000000110110101");
      break;
    }

    default:
      printf("ERROR");
      break;
  }

  printf("\n");

  return 0;
}
