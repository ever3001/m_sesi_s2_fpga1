#include "DCC.h"
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int
main(int argc, char const* argv[])
{
  uint16_t cmd = 0;
  printf("Test TRAIN_DIRECTION_BACKWARD\n");
  for (size_t i = 0; i < TRAIN_SPEED_END; ++i) {
    cmd = getTrainSpeedCmd((t_trainSpeed)i, TRAIN_DIRECTION_BACKWARD);
    printf("%s : ", trainSpeedArr[i]);
    printBinVal(cmd);
    printf("\n");
    // TODO: ASSERT
  }
  printf("Test TRAIN_DIRECTION_FORWARD\n");
  for (size_t i = 0; i < TRAIN_SPEED_END; ++i) {
    cmd = getTrainSpeedCmd((t_trainSpeed)i, TRAIN_DIRECTION_FORWARD);
    printf("%s : ", trainSpeedArr[i]);
    printBinVal(cmd);
    printf("\n");
    // TODO: ASSERT
  }

  printf("Test F0 to F4\n");
  cmd = 0;
  for (size_t i = TRAIN_FUNCTION_F00_LUMIERE_ON_OFF;
       i < TRAIN_FUNCTION_F04_TURBO_OFF + 1;
       ++i) {
    cmd |= getTrainFunctionCmd((t_trainFunctions)i, true);
    printf("%s : ", trainFunctionsArr[i]);
    printBinVal(cmd);
    printf("\n");
  }

  printf("Test F5 to F12\n");
  cmd = 0;
  for (size_t i = TRAIN_FUNCTION_F05_COMPRESSEUR;
       i < TRAIN_FUNCTION_F12_COURT_COR_FRANCAISE_2 + 1;
       ++i) {
    cmd = getTrainFunctionCmd((t_trainFunctions)i, true);
    printf("%s : ", trainFunctionsArr[i]);
    printBinVal(cmd);
    printf("\n");
  }

  printf("Test F13 to F20\n");
  cmd = 0;
  for (size_t i = TRAIN_FUNCTION_F13_ANNONCE_STATION_FRANCAISE_1;
       i < TRAIN_FUNCTION_F20_SABLE + 1;
       ++i) {
    cmd = getTrainFunctionCmd((t_trainFunctions)i, true);
    printf("%s : ", trainFunctionsArr[i]);
    printBinVal(cmd);
    printf("\n");
  }

  printf("Test F21\n");
  cmd = 0;
  cmd = getTrainFunctionCmd(TRAIN_FUNCTION_F21_LIBERATION_DES_FREINS, true);
  printf("%s : ", trainFunctionsArr[TRAIN_FUNCTION_F21_LIBERATION_DES_FREINS]);
  printBinVal(cmd);
  printf("\n");


  printf("Test other function number\n");
  cmd = 0;
  cmd = getTrainFunctionCmd(100, true);
  printf("Error function : ");
  printBinVal(cmd);
  printf("\n");


  printf("Test DCC Command Train Speed\n");
  int64_t DCCBuff = 0;
  DCCBuff = createTrainSpeedDCCCmd(2,4,TRAIN_SPEED_E_STOP,TRAIN_DIRECTION_FORWARD);
  printf("%" PRId64 "\n", DCCBuff);
  printBinVal(DCCBuff);

  printf("\n");


  printf("Test DCC Command Function F2\n");
  DCCBuff = 0;
  DCCBuff = createFunctionDCCCmd(4,TRAIN_FUNCTION_F02_COR_FRANCAISE_1,true);
  printf("%" PRId64 "\n", DCCBuff);
  printBinVal(DCCBuff);

  printf("\n");


  printf("Test DCC Command Function F14\n");
  DCCBuff = 0;
  DCCBuff = createFunctionDCCCmd(2,TRAIN_FUNCTION_F14_ANNONCE_STATION_FRANCAISE_2,true);
  printf("%" PRId64 "\n", DCCBuff);
  printBinVal(DCCBuff);

  printf("\n");

  return 0;
}