/**
 * @file TP_05.c
 * @author Joséphine MASINI, Apolline JOURET, Ever ATILANO
 * @brief Code pour piloter les trains via Microblaze et l?IP Centrale DCC
 * @version 0.1
 *
 */

#include "sleep.h" // for usleep()
#include "xgpio.h"
#include "xparameters.h"
#include "DCC.h"


#define GPIO_OUTPUT (0x0) // Pour declarer un GPIO comme sortie
#define GPIO_INPUT (0xF)  // Pour declarer un GPIO comme entrée

/*######## GPIO DEVIDE ID */
#define BUTTONS_DEVICE_ID // TODO: Find ID (p.e. XPAR_BOUTONS_DEVICE_ID)
#define LEDS_DEVICE_ID    // TODO: Find ID (p.e. XPAR_LED_SWITCH_DEVICE_ID)
/*########   GPIO PORTS */
#define PORT_BUTTONS (0x01)  // TODO: Verify the correct port
#define PORT_SWITCHES (0x02) // TODO: Verify the correct port
#define PORT_LEDS (0x01)     // TODO: Verify the correct port
/*########  GPIO_INT MASK */
#define BUTTONS_IR_CHANNEL XGPIO_IR_CH1_MASK // TODO: Verify the correct channel

/*######## INTERRUPT CONTROLLER ########*/
#define XINTC_ID XPAR_INTC_0_DEVICE_ID // TODO: Check this device id
#define IRQ_BOUTONS                                                            \
  XPAR_MICROBLAZE_0_AXI_INTC_BOUTONS_IP2INTC_IRPT_INTR // TODO: Check this
                                                       // definition
#define IRQ_SWITCHES                                                           \
  XPAR_MICROBLAZE_1_AXI_INTC_BOUTONS_IP2INTC_IRPT_INTR // TODO: Check this
                                                       // definition

  XGpio buttons; // Port qui contient les interrupteurs et les boutons lcr
XGpio leds;      // Port qui contient les leds
XIntc xintc;     // Interruption

void
buttonHandler(void* CallbackRef)
{
  int lcr_buttons_val = XGpio_DiscreteRead(&button, PORT_SWITCHES);
}

void
switchesHandler(void* CallbackRef)
{
  int switchesVal = XGpio_DiscreteRead(&button, PORT_BUTTONS);
}

void
initGPIO(void)
{
  /*** Initializer les objets XGpio */
  XGpio_Initialize(&buttons, BUTTONS_DEVICE_ID);
  XGpio_Initialize(&leds, LEDS_DEVICE_ID);
  /*** Declarer les boutons comme entrées et les leds comme sorties */
  XGpio_SetDataDirection(&buttons, PORT_BUTTONS, GPIO_INPUT);
  XGpio_SetDataDirection(&buttons, PORT_SWITCHES, GPIO_INPUT);
  XGpio_SetDataDirection(&led, PORT_LEDS, GPIO_OUTPUT);
  /*** Habiliter les interruptions des boutons */
  XGpio_InterruptGlobalEnable(&buttons);
  XGpio_InterruptEnable(&buttons, BUTTONS_IR_CHANNEL);
}

void
initInt(void)
{
  IRQ_BOUTONS
  IRQ_BOUTONS
  /*** Configurer et habiliter le controleur des interruptions pour les
   * boutons*/
  XIntc_Initialize(&xintc, XINTC_ID);
  XIntc_Connect(
    &xintc, IRQ_BOUTONS, (Xil_ExceptionHandler)buttonHandler, &buttons);
  XIntc_Enable(&xintc, IRQ_BOUTONS);

  // TODO: Check next two lines
  XIntc_Connect(
    &xintc, IRQ_SWITCHES, (Xil_ExceptionHandler)switchesHandler, &buttons);
  XIntc_Enable(&xintc, IRQ_SWITCHES);

  XIntc_Start(&xintc, XIN_REAL_MODE);
}

int
main()
{
  initGPIO();
  initInt();

  return 0;
}