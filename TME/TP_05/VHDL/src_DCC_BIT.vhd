--  DCC_BIT.vhd
-- ---------------------------------------------
--  DCC Bit. This module creates 1 or 0 to DCC
-- ---------------------------------------------
--  Version  : 1.0
--  Date     : March 2020
--  Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
    ENTITY DCC_BIT IS
-- ---------------------------------------------
  GENERIC (BIT : INTEGER RANGE 0 TO 1 := 1);
  PORT (
    Clk_100MHz : IN STD_LOGIC;  -- Clock input 100 Mhz
    Clk_1MHz   : IN STD_LOGIC;  -- Clock input 1 Mhz
    GoFlag     : IN STD_LOGIC;  -- GoFlag signal
    FinishFlag : OUT STD_LOGIC; -- End Signal
    DccOut     : OUT STD_LOGIC; -- DccOut Signal
    Reset      : IN STD_LOGIC   -- Async Reset
  );
END DCC_BIT;

-- ---------------------------------------------
    ARCHITECTURE Behavioral OF DCC_BIT IS
-- ---------------------------------------------
  --! ########          Constants         ########
  CONSTANT DEFAULT_VALUE           : INTEGER RANGE 0 TO 1                             := 0;                  -- Counter default value
  CONSTANT TIME_TO_COUNT_IN_US     : POSITIVE                                         := (100 - (BIT * 42)); -- 100 us for DCC_BIT_0 and 58 us for DCC_BIT_1

  --! ######## Signals for component ########
  SIGNAL InternalCounter           : INTEGER RANGE 0 TO (TIME_TO_COUNT_IN_US * 2) + 1 := DEFAULT_VALUE;      -- counter to 0 to (100/58) us
  SIGNAL Counting                  : STD_LOGIC                                        := LOW;
  SIGNAL CurrentState, FutureState : type_state                                       := S0;

BEGIN
  PROCESS (Clk_100MHz, Reset)
  BEGIN
    IF (Reset = HIGH) THEN
      CurrentState <= S0;
    ELSIF rising_edge (Clk_100MHz) THEN
      CurrentState <= FutureState;
    END IF;
  END PROCESS;

  PROCESS (Clk_1MHz, Counting)
  BEGIN
    IF Counting = HIGH THEN
      IF falling_edge (Clk_1MHz) THEN
        -- Increment the internal counter by 1
        InternalCounter <= InternalCounter + 1;
        IF InternalCounter >= (TIME_TO_COUNT_IN_US * 2) THEN
          InternalCounter <= DEFAULT_VALUE;
        END IF;
      END IF;

    ELSE
      InternalCounter <= DEFAULT_VALUE;
    END IF;
  END PROCESS;
  -- State Control;
  PROCESS (GoFlag, InternalCounter, FutureState, CurrentState)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 => FutureState <= S0;
        IF GoFlag = HIGH AND (InternalCounter = DEFAULT_VALUE) THEN
          FutureState <= S1;
        END IF;

      WHEN S1 => FutureState <= S1;
        IF InternalCounter >= (TIME_TO_COUNT_IN_US) THEN
          FutureState <= S2;
        END IF;

      WHEN S2 => FutureState <= S2;
        IF InternalCounter >= (TIME_TO_COUNT_IN_US * 2) THEN
          FutureState <= S0;
        END IF;

      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;

  -- Output control
  PROCESS (CurrentState)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 =>
        FinishFlag <= HIGH;
        DccOut     <= LOW;
        Counting   <= LOW;
      WHEN S1 =>
        FinishFlag <= LOW;
        DccOut     <= LOW;
        Counting   <= HIGH;
      WHEN S2 =>
        FinishFlag <= LOW;
        DccOut     <= HIGH;
        Counting   <= HIGH;

      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;

END ARCHITECTURE Behavioral;