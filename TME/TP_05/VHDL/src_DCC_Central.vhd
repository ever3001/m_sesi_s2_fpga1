
-- DCC_Central.vhd
-- ---------------------------------------------
-- State Machine
-- ---------------------------------------------
-- Version : 1.0
-- Date : March 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
ENTITY DCC_Central IS
  -- ---------------------------------------------
  GENERIC (NO_BITS : INTEGER := DCC_NO_BITS);
  PORT (
    Clk_100MHz      : IN STD_LOGIC;                     -- Clock input 100 Mhz
    DCC_SIGNAL_OUT  : OUT STD_LOGIC;                    -- DCC Out Signal
    LEDS_CMD        : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Leds to display the command
    LEDS_ADDR_TRAIN : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); -- Leds to display the train address
    BTN_LEFT        : IN STD_LOGIC;                     -- Left button
    BTN_CENTER      : IN STD_LOGIC;                     -- Center Button
    BTN_RIGHT       : IN STD_LOGIC;                     -- Right Button
    SW_ADDR_TRAIN   : IN STD_LOGIC_VECTOR(2 DOWNTO 0);  -- Switch to select the train number
    OUT_END_REG     : OUT STD_LOGIC;                    -- Indicates end reg
    Reset           : IN STD_LOGIC                      -- Async Reset
  );

END DCC_Central;

-- ---------------------------------------------
ARCHITECTURE Behavioral OF DCC_Central IS
  -- ---------------------------------------------
  --! ########   Signals for component    ########

  --! ######## Clock Divider
  SIGNAL Clk_1MHz : STD_LOGIC;
  --! ######## Tempo
  SIGNAL StartFlag   : STD_LOGIC := LOW;
  SIGNAL FinishFlag  : STD_LOGIC;
  SIGNAL TimeValInMs : BIT_08;
  --! ######## DCC_BIT_0
  SIGNAL BIT_0_GoFlag     : STD_LOGIC := LOW;
  SIGNAL BIT_0_FinishFlag : STD_LOGIC;
  SIGNAL BIT_0_DccOut     : STD_LOGIC;
  --! ######## DCC_BIT_1
  SIGNAL BIT_1_GoFlag     : STD_LOGIC := LOW;
  SIGNAL BIT_1_FinishFlag : STD_LOGIC;
  SIGNAL BIT_1_DccOut     : STD_LOGIC;
  --! ######## Registre DCC
  SIGNAL DCC_In      : DCC_COMMAND;
  SIGNAL DCC_bit_Out : STD_LOGIC;
  SIGNAL END_REG     : STD_LOGIC;
  SIGNAL COM_REG     : STD_LOGIC_VECTOR(1 DOWNTO 0);
  --! ######## Générateur Trames de Test
  SIGNAL Trame_DCC_OUT : DCC_COMMAND;

BEGIN
  CLOCK_DIV_0 : ENTITY work.ClockDiv(BEHAVIORAL)
    GENERIC MAP(DIVIDING_FACTOR => CLOCK_DIVIDING_FACTOR)
    PORT MAP(
      ClockIn  => Clk_100MHz,
      ClockOut => Clk_1MHz,
      Reset    => Reset
    );

  TEMPO_0 : ENTITY work.Tempo(BEHAVIORAL)
    PORT MAP(
      Clk_1MHz    => Clk_1MHz,
      StartFlag   => StartFlag,
      FinishFlag  => FinishFlag,
      TimeValInMs => TimeValInMs,
      Reset       => Reset
    );

  DCC_BIT_0 : ENTITY work.DCC_BIT(BEHAVIORAL)
    GENERIC MAP(BIT => 0)
    PORT MAP(
      Clk_100MHz => Clk_100MHz,
      Clk_1MHz   => Clk_1MHz,
      GoFlag     => BIT_0_GoFlag,
      FinishFlag => BIT_0_FinishFlag,
      DccOut     => BIT_0_DccOut,
      Reset      => Reset
    );

  DCC_BIT_1 : ENTITY work.DCC_BIT(BEHAVIORAL)
    GENERIC MAP(BIT => 1)
    PORT MAP(
      Clk_100MHz => Clk_100MHz,
      Clk_1MHz   => Clk_1MHz,
      GoFlag     => BIT_1_GoFlag,
      FinishFlag => BIT_1_FinishFlag,
      DccOut     => BIT_1_DccOut,
      Reset      => Reset
    );

  REG_DCC_0 : ENTITY work.RegistreDCC(BEHAVIORAL)
    GENERIC MAP(NO_BITS => NO_BITS)
    PORT MAP(
      Clk_100MHz  => Clk_100MHz,
      DCC_In      => DCC_In,
      DCC_bit_Out => DCC_bit_Out,
      END_REG     => END_REG,
      COM_REG     => COM_REG,
      Reset       => Reset
    );

  TRAME_DCC_0 : ENTITY work.Trame_DCC(impl)
    PORT MAP(
      Clk_100       => Clk_100MHz,
      Led_cmd       => LEDS_CMD,
      Led_addr      => LEDS_ADDR_TRAIN,
      Bouton_L      => BTN_LEFT,
      Bouton_C      => BTN_CENTER,
      Bouton_R      => BTN_RIGHT,
      SWITCHS       => SW_ADDR_TRAIN,
      Trame_DCC_OUT => DCC_In,
      Reset         => Reset
    );

  MAE_0 : ENTITY work.MAE(BEHAVIORAL)
    GENERIC MAP(NO_BITS => NO_BITS)
    PORT MAP(
      Clk_100MHz           => Clk_100MHz,
      REG_END_REG          => END_REG,
      REG_COM_REG          => COM_REG,
      REG_DCC_BIT_OUT      => DCC_bit_Out,
      TMPO_StartFlag       => StartFlag,
      TMPO_FinishFlag      => FinishFlag,
      TMPO_TimeValInMs     => TimeValInMs,
      DCC_BIT_0_GoFlag     => BIT_0_GoFlag,
      DCC_BIT_0_FinishFlag => BIT_0_FinishFlag,
      DCC_BIT_1_GoFlag     => BIT_1_GoFlag,
      DCC_BIT_1_FinishFlag => BIT_1_FinishFlag,
      Reset                => Reset
    );

  DCC_SIGNAL_OUT <= BIT_0_DccOut OR BIT_1_DccOut;
  OUT_END_REG    <= END_REG;

END ARCHITECTURE Behavioral;