
-- MAE.vhd
-- ---------------------------------------------
-- State Machine
-- ---------------------------------------------
-- Version : 1.0
-- Date : March 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
ENTITY MAE IS
  -- ---------------------------------------------
  GENERIC (NO_BITS : INTEGER := DCC_NO_BITS);
  PORT (
    Clk_100MHz                       : IN STD_LOGIC;                     -- Clock input 100 Mhz
    --! ######## Registre
    REG_END_REG                      : IN STD_LOGIC;                     -- Flag pour indiquer si le registre a finit de envoyer la trame ou pas
    REG_COM_REG                      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); -- Commande le multiplexeur qui gère le registre, on a ici 4 possibilitées ou 3
    REG_DCC_BIT_OUT                  : IN STD_LOGIC;                     -- DCC Command Out. Sort qu'un bit à la fois, pas la peine d'avoir un vector
    --! ######## Tempo
    TMPO_StartFlag                   : OUT STD_LOGIC;                    -- Starts to count
    TMPO_FinishFlag                  : IN STD_LOGIC;                     -- End Signal
    TMPO_TimeValInMs                 : OUT BIT_08;                       -- InternalCounter value in micro seconds
    --! ######## DCC_BIT_0
    DCC_BIT_0_GoFlag                 : OUT STD_LOGIC;                    -- GoFlag signal
    DCC_BIT_0_FinishFlag             : IN STD_LOGIC;                     -- End Signal
    --! ######## DCC_BIT_1
    DCC_BIT_1_GoFlag                 : OUT STD_LOGIC;                    -- GoFlag signal
    DCC_BIT_1_FinishFlag             : IN STD_LOGIC;                     -- End Signal

    Reset                            : IN STD_LOGIC                      -- Async Reset
  );
END MAE;

-- ---------------------------------------------
ARCHITECTURE Behavioral OF MAE IS
  -- ---------------------------------------------

  --! ######## Signals for component ########
  SIGNAL CurrentState, FutureState : TYPE_STATE                   := S0;

  --! ######## Constants ########
  CONSTANT CMD_REG_SAVE_VALUE      : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
  CONSTANT CMD_REG_SHIFT_VALUE     : STD_LOGIC_VECTOR(1 DOWNTO 0) := "01";
  CONSTANT CMD_REG_KEEP_VALUE      : STD_LOGIC_VECTOR(1 DOWNTO 0) := "10";

BEGIN
  --1er process : Process du registre d'états
  PROCESS (Clk_100MHz, Reset)
  BEGIN
    IF (Reset = HIGH) THEN
      CurrentState <= S0;
    ELSIF rising_edge (Clk_100MHz) THEN
      CurrentState <= FutureState;
    END IF;
  END PROCESS;
  --2eme process : Combinatoire des etats
  PROCESS (CurrentState, REG_END_REG, REG_DCC_BIT_OUT, DCC_BIT_0_FinishFlag, DCC_BIT_1_FinishFlag, TMPO_FinishFlag)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 => FutureState <= S0;
        IF (REG_END_REG = LOW) THEN
          FutureState <= S1;
        END IF;

      WHEN S1 => FutureState <= S1;
        IF (REG_END_REG = HIGH) THEN
          FutureState <= S5;
        ELSIF (REG_DCC_BIT_OUT = HIGH) THEN
          FutureState <= S3;
        ELSIF (REG_DCC_BIT_OUT = LOW) THEN
          FutureState <= S2;
        END IF;

      WHEN S2 => FutureState <= S2;
        IF (DCC_BIT_0_FinishFlag = LOW) THEN
          FutureState <= S4;
        END IF;

      WHEN S3 => FutureState <= S3;
        IF (DCC_BIT_1_FinishFlag = LOW) THEN
          FutureState <= S4;
        END IF;

      WHEN S4 => FutureState <= S4;
        IF (DCC_BIT_1_FinishFlag = HIGH AND DCC_BIT_0_FinishFlag = HIGH) THEN
          FutureState <= S1;
        END IF;

      WHEN S5 => FutureState <= S5;
        IF (rising_edge(TMPO_FinishFlag)) THEN
          FutureState <= S0;
        END IF;

      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;

  --3eme process : Combinatoire des sorties
  PROCESS (CurrentState)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 =>
        --! Outputs
        TMPO_StartFlag   <= LOW;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_1_GoFlag <= LOW;
        DCC_BIT_0_GoFlag <= LOW;
        REG_COM_REG      <= CMD_REG_SAVE_VALUE;

      WHEN S1 =>
        --! Outputs
        TMPO_StartFlag   <= LOW;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_1_GoFlag <= LOW;
        DCC_BIT_0_GoFlag <= LOW;
        REG_COM_REG      <= CMD_REG_KEEP_VALUE;

        --! #### Activate DCC_BIT_0
      WHEN S2 =>
        --! Outputs
        TMPO_StartFlag   <= LOW;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_0_GoFlag <= HIGH;
        DCC_BIT_1_GoFlag <= LOW;
        REG_COM_REG      <= CMD_REG_SHIFT_VALUE;

        --! #### Activate DCC_BIT_1
      WHEN S3 =>
        --! Outputs
        TMPO_StartFlag   <= LOW;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_0_GoFlag <= LOW;
        DCC_BIT_1_GoFlag <= HIGH;
        REG_COM_REG      <= CMD_REG_SHIFT_VALUE;

        --! ### Wait for finish DCC_BIT
      WHEN S4 =>
        --! Outputs
        TMPO_StartFlag   <= LOW;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_0_GoFlag <= LOW;
        DCC_BIT_1_GoFlag <= LOW;
        REG_COM_REG      <= CMD_REG_KEEP_VALUE;

        --! #### Activate TEMPO
      WHEN S5 =>
        --! Outputs
        TMPO_StartFlag   <= HIGH;
        TMPO_TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS;
        DCC_BIT_0_GoFlag <= LOW;
        DCC_BIT_1_GoFlag <= LOW;
        REG_COM_REG      <= CMD_REG_SAVE_VALUE;

      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;
END ARCHITECTURE Behavioral;