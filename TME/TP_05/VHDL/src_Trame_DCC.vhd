-- Trame_DCC.vhd
-- ---------------------------------------------
-- Generates a DCC command
-- ---------------------------------------------
-- Version : 1.0
-- Date : May 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
ENTITY Trame_DCC IS
  -- ---------------------------------------------
  PORT (
    Clk_100       : IN STD_LOGIC;
    Reset         : IN STD_LOGIC;
    Trame_DCC_OUT : OUT DCC_COMMAND;
    SWITCHS       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    Bouton_L      : IN STD_LOGIC;
    Bouton_R      : IN STD_LOGIC;
    Bouton_C      : IN STD_LOGIC;
    Led_addr      : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    Led_cmd       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END Trame_DCC;

-- ---------------------------------------------
ARCHITECTURE impl OF Trame_DCC IS
  -- ---------------------------------------------

  --Signaux des Leds pour l'affichage de sortie 
  SIGNAL num_command   : STD_LOGIC_VECTOR(3 DOWNTO 0) := "0001";
  SIGNAL prev_Bouton_L : STD_LOGIC;
  SIGNAL prev_Bouton_R : STD_LOGIC;
  SIGNAL prev_Bouton_C : STD_LOGIC;
  SIGNAL cpt           : UNSIGNED(3 DOWNTO 0) := "0001";
  SIGNAL COMD_1ER      : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL COMD_2EME     : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL Controle      : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL ADDR          : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL ERROR         : STD_LOGIC;   -- pour valider que la commande est valide, évite de sortir une commande erronée
  SIGNAL sig_DCC       : DCC_COMMAND; -- Calcul de la trame avant le registre de sortie
  SIGNAL sig_DCC_reg   : DCC_COMMAND; -- Signal à mettre dans le registre pour le reporter ensuite avec le port de sortie
BEGIN
  --On fait un process pour récupérer la valeur de la commande choisie en appuyant sur les boutons
  PROCESS (Clk_100, Reset)
  BEGIN
    IF (Reset = '1') THEN cpt <= "0001"; sig_DCC_reg <= (OTHERS => '0');
    ELSIF falling_edge(Clk_100) THEN
      IF ((ERROR = '0') AND (Bouton_C = '1') AND (prev_Bouton_C = '0')) THEN 
        sig_DCC_reg <= sig_DCC;
      ELSIF ((Bouton_R = '1') AND (prev_Bouton_R = '0')) THEN
        IF (cpt = "1001") THEN cpt <= "0001";--eviter de tomber en erreur car on a rien au dessus de 1001
        ELSE cpt <= cpt + 1;
        END IF;
      ELSIF ((Bouton_L = '1') AND (prev_Bouton_L = '0')) THEN
        IF (cpt = "0001") THEN cpt <= "1001";--eviter de tomber en erreur avec les valeurs négatives, on a rien en dessous de 0000
        ELSE cpt <= cpt - 1;
        END IF;
      ELSE 
        sig_DCC_reg <= sig_DCC_reg;
        cpt <= cpt;
      END IF;
    END IF;
    num_command <= std_logic_vector(cpt); 
    prev_Bouton_L <= Bouton_L;
    prev_Bouton_R <= Bouton_R;
    prev_Bouton_C <= Bouton_C;
  END PROCESS;


  PROCESS (num_command)--process combinatoire qui ne dépend pas de Clk_100, mais d'un autre signal
  BEGIN
    ERROR <= '0';
    CASE(num_command) IS

      WHEN "0001" => COMD_1ER <= "01100100";COMD_2EME <= "01100100";  -- Vitesse plus lente
      WHEN "0010" => COMD_1ER <= "01111111";COMD_2EME <= "01111111";  -- Vitesse plus rapide
      WHEN "0011" => COMD_1ER <= "01100000";COMD_2EME <= "01100000";  -- Arrét du train
      WHEN "0100" => COMD_1ER <= "10010000";COMD_2EME <= "10010000";  -- Allumer les phares
      WHEN "0101" => COMD_1ER <= "10000000";COMD_2EME <= "10000000";  -- Eteindte les phares
      WHEN "0110" => COMD_1ER <= "10100100";COMD_2EME <= "10100100";  -- Activer le klaxon
      WHEN "0111" => COMD_1ER <= "10100000";COMD_2EME <= "10100000";  -- Désactiver le klaxon
      WHEN "1000" => COMD_1ER <= "11011110";COMD_2EME <= "00000001";  -- Annonce d'entrée en gare
      WHEN "1001" => COMD_1ER <= "11011110";COMD_2EME <= "00000000";  -- Désactivation de l'annonce d'entrée en gare
      WHEN OTHERS => COMD_1ER <= "00000000";COMD_2EME <= "00000000"; ERROR <= '1';

    END CASE;
  END PROCESS;

  --Calcul du champs d'adresse
  ADDR     <= "00000" & SWITCHS;

  --Equation  du champs de contrôle avec les XOR
  Controle <= ADDR XOR COMD_1ER XOR COMD_2EME;

  -- Calcul de la trame de sortie 
  sig_DCC  <= "11111111111111" & '0' & ADDR & '0' & COMD_1ER & '0' & COMD_2EME & '0' & Controle & '1';

  --Attribution du signal au port de sortie 
  Trame_DCC_OUT <= sig_DCC_reg;
  Led_addr      <= SWITCHS;
  Led_cmd       <= num_command;
END impl;