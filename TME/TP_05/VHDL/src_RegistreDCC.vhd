-- RegistreDCC.vhd
-- ---------------------------------------------
-- Save a DCC command
-- ---------------------------------------------
-- Version : 1.0
-- Date : March 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
    ENTITY RegistreDCC IS
-- ---------------------------------------------
  GENERIC (NO_BITS : INTEGER := DCC_NO_BITS);
  PORT (
    Clk_100MHz  : IN STD_LOGIC;                    -- Clock input 100 Mhz
    DCC_In      : IN DCC_COMMAND;                  -- DCC Command In
    DCC_bit_Out : OUT STD_LOGIC;                   -- DCC Command Out. Sort qu'un bit à la fois, pas la peine d'avoir un vector
    END_REG     : OUT STD_LOGIC;                   -- Flag pour indiquer si le registre a finit de envoyer la trame ou pas
    COM_REG     : IN STD_LOGIC_VECTOR(1 DOWNTO 0); -- Commande le multiplexeur qui gère le registre, on a ici 4 possibilitées ou 3
    Reset       : IN STD_LOGIC                     -- Async Reset
  );

END RegistreDCC;

-- ---------------------------------------------
    ARCHITECTURE Behavioral OF RegistreDCC IS
-- ---------------------------------------------
  --! ######## Signals for component ########
  SIGNAL DCC_CommandVal : DCC_COMMAND                := DCC_CMD_DEFAULT_VALUE;--signal reg, le registre de données interne, la sortie des bascules D
  SIGNAL NO_BITS_SENT   : INTEGER RANGE 0 TO NO_BITS := 0;
  SIGNAL SHIFTED        : STD_LOGIC                  := LOW;

BEGIN

  REG_PROCESS :
  PROCESS (Clk_100MHz, Reset)--process du registre
  BEGIN
    IF (Reset = HIGH) THEN -- On a ici le reset asynchrone
      DCC_CommandVal <= DCC_CMD_DEFAULT_VALUE;
    ELSIF rising_edge (Clk_100MHz) THEN -- Sur front montant, on applique les commandes suivantes selon les cas
      CASE(COM_REG) IS

        WHEN "00"   => DCC_CommandVal   <= DCC_In;                                     -- COM_REG = "00", on charge l'entrée dans le registre de données interne, 1ere commande à faire avant de décaler
        WHEN "01"   => 
        IF (SHIFTED = LOW) THEN
          DCC_CommandVal   <= DCC_CommandVal(NO_BITS - 2 DOWNTO 0) & '0'; -- COM_REG = "01", On décale en ajoutant un 0 au 8e bit et on décale les autres
        END IF;
        WHEN OTHERS => DCC_CommandVal   <= DCC_CommandVal;                             -- COM_REG = "11", On mémorise l'état, on ne décale pas la valeur
        -- On peut rajouter un reset synchrone mais il est effectué quand il n'y a que des 0 à la fin sur la trame et que les 61 BITS ont été transmis --

      END CASE;
    END IF;
  END PROCESS;

  NO_BITS_SENT_PROCESS :
  PROCESS (Clk_100MHz, Reset) --On fait un compteur qui permet d'indiquer la fin de l'émission de la trame
  BEGIN
    IF rising_edge(Clk_100MHz) THEN -- Compteur synchrone définit sur front montant, les 2 process sont synchronisés
      IF (COM_REG = "01" AND NO_BITS_SENT < NO_BITS AND SHIFTED = LOW) THEN
        NO_BITS_SENT <= NO_BITS_SENT + 1; --incrémentation du compteur quand on fait un shift
        SHIFTED      <= HIGH;

      ELSIF (COM_REG = "00") THEN
        NO_BITS_SENT <= 0; --reset du compteur quand on charge une nouvelle donnée dans le registre
        SHIFTED      <= LOW;
      ELSE
        NO_BITS_SENT <= NO_BITS_SENT; --garde la valeur du compteur au moment de la mémorisation
        SHIFTED      <= LOW;
      END IF;
    END IF;
  END PROCESS;

  END_REG     <= '1' WHEN NO_BITS_SENT = NO_BITS ELSE '0'; -- comparateur de sortie pour signifier que la trame a été émise en entier

  DCC_bit_Out <= DCC_CommandVal(NO_BITS - 1);

END ARCHITECTURE Behavioral;