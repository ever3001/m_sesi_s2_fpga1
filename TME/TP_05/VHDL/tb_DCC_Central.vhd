-- ----------------------------------------
-- VHDL Test Bench for the State Machine
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_DCC_Central IS END;

ARCHITECTURE TEST OF TB_DCC_Central IS

  --! ########   Signals for component    ########
  SIGNAL Clk_100MHz : STD_LOGIC := LOW;
  SIGNAL Reset      : STD_LOGIC := HIGH;

  SIGNAL DCC_SIGNAL_OUT  : STD_LOGIC;
  SIGNAL LEDS_CMD        : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL LEDS_ADDR_TRAIN : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL BTN_LEFT        : STD_LOGIC                    := LOW;
  SIGNAL BTN_CENTER      : STD_LOGIC                    := LOW;
  SIGNAL BTN_RIGHT       : STD_LOGIC                    := LOW;
  SIGNAL SW_ADDR_TRAIN   : STD_LOGIC_VECTOR(2 DOWNTO 0) := "001";
  SIGNAL OUT_END_REG     : STD_LOGIC;

  SIGNAL STOP_SIM     : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST         : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := LOW; -- Indicates if an assert is incorrect

  --! ########      Constants        ########
  CONSTANT USER_TIME_SIM : TIME := 1 us;

BEGIN
  DCC_CENTRAL_0 : ENTITY work.DCC_Central(BEHAVIORAL)
    GENERIC MAP(NO_BITS => DCC_NO_BITS)
    PORT MAP(
      Clk_100MHz      => Clk_100MHz,
      DCC_SIGNAL_OUT  => DCC_SIGNAL_OUT,
      LEDS_CMD        => LEDS_CMD,
      LEDS_ADDR_TRAIN => LEDS_ADDR_TRAIN,
      BTN_LEFT        => BTN_LEFT,
      BTN_CENTER      => BTN_CENTER,
      BTN_RIGHT       => BTN_RIGHT,
      SW_ADDR_TRAIN   => SW_ADDR_TRAIN,
      OUT_END_REG     => OUT_END_REG,
      Reset           => Reset
    );

  Clk_100MHz <= NOT(Clk_100MHz) AFTER (CLOCK_PERIODE_IN_NS/2) WHEN STOP_SIM = LOW;

  PROCESS
  BEGIN
    Reset <= HIGH;
    WAIT UNTIL Clk_100MHz = HIGH;
    WAIT UNTIL Clk_100MHz = HIGH;
    Reset <= LOW;
    WAIT UNTIL OUT_END_REG = HIGH;
    REPORT "[TEST_01] INIT : Send First command to train 4";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    SW_ADDR_TRAIN <= "100"; -- Train 4
    BTN_LEFT      <= '0';
    BTN_CENTER    <= '0';
    BTN_RIGHT     <= '0';
    WAIT UNTIL Clk_100MHz = HIGH;
    BTN_RIGHT <= HIGH;
    WAIT FOR USER_TIME_SIM;
    BTN_RIGHT <= LOW;
    WAIT FOR USER_TIME_SIM;
    BTN_CENTER <= HIGH;
    WAIT FOR USER_TIME_SIM;
    BTN_CENTER <= LOW;
    WAIT FOR USER_TIME_SIM;

    WAIT UNTIL OUT_END_REG = HIGH;

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";

    WAIT FOR USER_TIME_SIM * 2;

    REPORT "[TEST_02] INIT : Send Second command to train 2";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    SW_ADDR_TRAIN <= "010"; -- Train 2
    BTN_RIGHT     <= HIGH;
    WAIT FOR USER_TIME_SIM;
    BTN_RIGHT <= LOW;
    WAIT FOR USER_TIME_SIM;
    BTN_CENTER <= HIGH;
    WAIT FOR USER_TIME_SIM;
    BTN_CENTER <= LOW;
    WAIT FOR USER_TIME_SIM;

    WAIT UNTIL OUT_END_REG = HIGH;

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_02] FINISHED";

    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;