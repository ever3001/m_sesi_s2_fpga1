-- ----------------------------------------
-- VHDL Test Bench for Clock Divider
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_CLOCKDIV IS END;

ARCHITECTURE TEST OF TB_CLOCKDIV IS
  --! ########   Signals for component    ########
  SIGNAL ClockIn      : STD_LOGIC := LOW;
  SIGNAL ClockOut     : STD_LOGIC;
  SIGNAL Reset        : STD_LOGIC := HIGH;

  SIGNAL STOP_SIM     : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST         : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := LOW; -- Indicates if an assert is incorrect

BEGIN
  CLOCK_DIV_0 : ENTITY work.ClockDiv(BEHAVIORAL)
    GENERIC MAP(DIVIDING_FACTOR => CLOCK_DIVIDING_FACTOR)
    PORT MAP(
      ClockIn  => ClockIn,
      ClockOut => ClockOut,
      Reset    => Reset
    );

  ClockIn <= NOT(ClockIn) AFTER (CLOCK_PERIODE_IN_NS/2) WHEN STOP_SIM = LOW;

  PROCESS
    VARIABLE clockOutFlag : STD_LOGIC := LOW;
  BEGIN
    WAIT UNTIL ClockIn = HIGH;
    Reset <= LOW;

    REPORT "[TEST_01] INIT : Test if the divisor works";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    clockOutFlag := LOW;
    L_T01_01 : FOR i IN 1 TO 100 LOOP
      WAIT FOR ((CLOCK_PERIODE_IN_NS * CLOCK_DIVIDING_FACTOR) / 2);
      ASSERT (ClockOut = clockOutFlag) REPORT "[TEST_01][Error]: In ClockOut val. ClockOut = " & INTEGER'image(bit_to_integer(ClockOut)) SEVERITY SEVERITY_LVL;
      IF NOT(ClockOut = clockOutFlag) THEN
        ERROR_SIGNAL <= HIGH;
      ELSE
        ERROR_SIGNAL <= LOW;
      END IF;
      clockOutFlag := NOT clockOutFlag;
    END LOOP; -- L_T01_01
    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";

    Reset <= HIGH;
    WAIT UNTIL ClockIn = HIGH;
    Reset <= LOW;
    WAIT UNTIL ClockIn = HIGH;
    REPORT "[TEST_02] INIT : Test the reset";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    WAIT FOR ((CLOCK_PERIODE_IN_NS * CLOCK_DIVIDING_FACTOR /2));
    ASSERT (ClockOut = HIGH) REPORT "[TEST_02][Error]: Clockout != 1" SEVERITY SEVERITY_LVL;
    IF NOT(ClockOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;
    Reset <= HIGH;
    WAIT UNTIL ClockIn = HIGH;
    Reset <= LOW;
    WAIT UNTIL ClockIn = HIGH;
    ASSERT (ClockOut = LOW) REPORT "[TEST_02][Error]: Clockout != 0" SEVERITY SEVERITY_LVL;
    IF NOT(ClockOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;
    TEST <= LOW;
    REPORT "[TEST_02] FINISHED";

    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;