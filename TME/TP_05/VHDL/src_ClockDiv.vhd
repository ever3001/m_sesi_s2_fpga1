-- ClockDiv.vhd
-- ---------------------------------------------
-- Clock Divider (Frequency Divider)
-- ---------------------------------------------
-- Version : 1.0
-- Date : March 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

-- ---------------------------------------------
    ENTITY ClockDiv IS
-- ---------------------------------------------
  GENERIC (DIVIDING_FACTOR : INT_BIT_10 := CLOCK_DIVIDING_FACTOR);
  PORT (
    ClockIn  : IN STD_LOGIC;  -- Clock input
    ClockOut : OUT STD_LOGIC; -- Clock output
    Reset    : IN STD_LOGIC   -- Async Reset
  );
END ClockDiv;

-- ---------------------------------------------
    ARCHITECTURE Behavioral OF ClockDiv IS
-- ---------------------------------------------
  --! ########      Constants        ########
  CONSTANT MAX_RANGE : INT_BIT_10 := (DIVIDING_FACTOR/2);

  --! ######## Signals for component ########
  SIGNAL Count       : INTEGER RANGE 0 TO (MAX_RANGE + 1);
  SIGNAL Temp        : STD_LOGIC := LOW;

BEGIN
  PROCESS (ClockIn, Reset)
  BEGIN
    IF (Reset = HIGH) THEN
      Count <= 0;
      Temp  <= LOW;
    ELSIF rising_edge (ClockIn) THEN
      Count <= Count + 1;
      IF (Count = MAX_RANGE - 1) THEN
        Temp  <= NOT(Temp);
        Count <= 0;
      END IF;
    END IF;
  END PROCESS;
  ClockOut <= Temp;
END ARCHITECTURE Behavioral;