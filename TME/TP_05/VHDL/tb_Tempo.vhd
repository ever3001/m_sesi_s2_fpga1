-- ----------------------------------------
-- VHDL Test Bench for Tempo
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE ieee.math_real.uniform;
USE ieee.math_real.floor;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_TEMPO IS END;

ARCHITECTURE TEST OF TB_TEMPO IS

  --! ########   Signals for component    ########
  SIGNAL Clk_1MHz : STD_LOGIC := LOW;

  SIGNAL StartFlag   : STD_LOGIC := LOW;
  SIGNAL FinishFlag  : STD_LOGIC;
  SIGNAL TimeValInMs : BIT_08    := (OTHERS => LOW);
  SIGNAL Reset       : STD_LOGIC := HIGH;

  SIGNAL STOP_SIM     : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST         : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := LOW; -- Indicates if an assert is incorrect

BEGIN
  TEMPO_0 : ENTITY work.Tempo(BEHAVIORAL)
    PORT MAP(
      Clk_1MHz    => Clk_1MHz,
      StartFlag   => StartFlag,
      FinishFlag  => FinishFlag,
      TimeValInMs => TimeValInMs,
      Reset       => Reset
    );

  Clk_1MHz <= NOT(Clk_1MHz) AFTER (CLOCK_PERIODE_1MHZ_IN_US/2) WHEN STOP_SIM = LOW;

  PROCESS
    VARIABLE clockOutFlag : STD_LOGIC := LOW;

    -- ############ Variables for the random number
    VARIABLE seed1 : POSITIVE;
    VARIABLE seed2 : POSITIVE;
    VARIABLE x     : real;
    VARIABLE y     : INTEGER;
    -- ############
  BEGIN
    -- ############ Make a Reset to put all values to default
    Reset <= HIGH;
    WAIT UNTIL Clk_1MHz = HIGH;
    Reset <= LOW;
    WAIT UNTIL Clk_1MHz = HIGH;
    -- ############

    REPORT "[TEST_01] INIT : Test if the tempo could count to 6 ms";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    clockOutFlag := LOW;
    -- ############
    TimeValInMs <= TEMPO_DEFAULT_TIME_IN_MS; -- 6 ms
    StartFlag   <= HIGH;
    WAIT UNTIL Clk_1MHz = HIGH;
    StartFlag <= LOW;
    WAIT FOR 2 us;
    ASSERT (FinishFlag = LOW) REPORT "[TEST_01][Error]: FinishFlag is not 0, is = " & INTEGER'image(bit_to_integer(FinishFlag)) SEVERITY SEVERITY_LVL;
    IF NOT(FinishFlag = LOW) THEN ERROR_SIGNAL <= HIGH;
    ELSE ERROR_SIGNAL                          <= LOW;
    END IF;

    WAIT FOR 6 ms;

    ASSERT (FinishFlag = HIGH) REPORT "[TEST_01][Error]: FinishFlag is not 1" SEVERITY SEVERITY_LVL;
    IF NOT(FinishFlag = HIGH) THEN ERROR_SIGNAL <= HIGH;
    ELSE ERROR_SIGNAL                           <= LOW;
    END IF;

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";

    -- ############ Make a Reset to put all values to default
    Reset <= HIGH;
    WAIT UNTIL Clk_1MHz = HIGH;
    Reset <= LOW;
    WAIT UNTIL Clk_1MHz = HIGH;
    -- ############

    REPORT "[TEST_02] INIT : Random numbers";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    clockOutFlag := LOW;
    -- ############

    L_T02_01 : FOR i IN 0 TO 10 LOOP
      -- Calcul the first random number
      uniform(seed1, seed2, x);
      y := INTEGER(floor(x * 32.0));
      TimeValInMs <= to_unsigned(y, TimeValInMs'length);

      StartFlag <= HIGH;
      WAIT UNTIL Clk_1MHz = HIGH;
      StartFlag <= LOW;
      WAIT FOR 2 us;
      ASSERT (FinishFlag = LOW) REPORT "[TEST_02][Error]: FinishFlag is not 0, is = " & INTEGER'image(bit_to_integer(FinishFlag)) SEVERITY SEVERITY_LVL;
      IF NOT(FinishFlag = LOW) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                          <= LOW;
      END IF;

      WAIT FOR (1 ms * y);

      ASSERT (FinishFlag = HIGH) REPORT "[TEST_02][Error]: FinishFlag is not 1" SEVERITY SEVERITY_LVL;
      IF NOT(FinishFlag = HIGH) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                           <= LOW;
      END IF;

    END LOOP; -- L_T02_01

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_02] FINISHED";
    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;