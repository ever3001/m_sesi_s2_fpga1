-- ----------------------------------------
-- VHDL Test Bench for the DCC register
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_RegistreDCC IS END;

ARCHITECTURE TEST OF TB_RegistreDCC IS
  --! ########   Signals for component    ########

  SIGNAL Clk_100MHz : STD_LOGIC := LOW;
  SIGNAL Clk_1MHz   : STD_LOGIC;
  SIGNAL Reset      : STD_LOGIC := HIGH;

  SIGNAL DCC_In      : DCC_COMMAND := DCC_CMD_DEFAULT_VALUE; -- DCC Command In
  SIGNAL DCC_bit_Out : STD_LOGIC;                            -- DCC Command Out. Sort qu'un bit à la fois, pas la peine d'avoir un vector
  SIGNAL END_REG     : STD_LOGIC;                            -- Flag pour indiquer si le registre a finit de envoyer la trame ou pas
  SIGNAL COM_REG     : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- Commande le multiplexeur qui gère le registre, on a ici 4 possibilitées ou 3

  SIGNAL STOP_SIM     : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST         : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := LOW; -- Indicates if an assert is incorrect
BEGIN
  CLOCK_DIV_0 : ENTITY work.ClockDiv(BEHAVIORAL)
    GENERIC MAP(DIVIDING_FACTOR => CLOCK_DIVIDING_FACTOR)
    PORT MAP(
      ClockIn  => Clk_100MHz,
      ClockOut => Clk_1MHz,
      Reset    => Reset
    );

  REG_DCC_0 : ENTITY work.RegistreDCC(BEHAVIORAL)
    GENERIC MAP(NO_BITS => DCC_NO_BITS)
    PORT MAP(
      Clk_100MHz  => Clk_100MHz,
      DCC_In      => DCC_In,
      DCC_bit_Out => DCC_bit_Out,
      END_REG     => END_REG,
      COM_REG     => COM_REG,
      Reset       => Reset
    );

  Clk_100MHz <= NOT(Clk_100MHz) AFTER (CLOCK_PERIODE_IN_NS/2) WHEN STOP_SIM = LOW;

  PROCESS
  BEGIN
    Reset <= HIGH;
    WAIT UNTIL Clk_100MHz = HIGH;
    Reset <= LOW;
    WAIT UNTIL Clk_100MHz = HIGH;
    REPORT "[TEST_01] INIT : Test DCC_CMD_VITESSE_LENTE_STEP_5 Command";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    DCC_In  <= DCC_CMD_VITESSE_LENTE_STEP_5;
    COM_REG <= "00";
    WAIT UNTIL Clk_100MHz = HIGH;

    L_T01_01 : FOR i IN (DCC_NO_BITS - 1) DOWNTO 0 LOOP
      COM_REG <= "11";
      WAIT UNTIL Clk_100MHz = HIGH;
      ASSERT (DCC_bit_Out = DCC_CMD_VITESSE_LENTE_STEP_5(i)) REPORT "[TEST_01][Error]: DCC_bit_Out is not the bit expected" SEVERITY SEVERITY_LVL;
      IF NOT(DCC_bit_Out = DCC_CMD_VITESSE_LENTE_STEP_5(i)) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                                                       <= LOW;
      END IF;
      COM_REG <= "01";
      WAIT UNTIL Clk_100MHz = HIGH;

    END LOOP; -- L_T01_01

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";
    WAIT FOR 1 us;
    REPORT "[TEST_02] INIT : Test DCC_CMD_VITESSE_PLUS_RAPIDE_STEP_28 Command";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    DCC_In  <= DCC_CMD_VITESSE_PLUS_RAPIDE_STEP_28;
    COM_REG <= "00";
    WAIT UNTIL Clk_100MHz = HIGH;

    L_T02_01 : FOR i IN (DCC_NO_BITS - 1) DOWNTO 0 LOOP
      COM_REG <= "11";
      WAIT UNTIL Clk_100MHz = HIGH;
      ASSERT (DCC_bit_Out = DCC_CMD_VITESSE_PLUS_RAPIDE_STEP_28(i)) REPORT "[TEST_02][Error]: DCC_bit_Out is not the bit expected" SEVERITY SEVERITY_LVL;
      IF NOT(DCC_bit_Out = DCC_CMD_VITESSE_PLUS_RAPIDE_STEP_28(i)) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                                                              <= LOW;
      END IF;
      COM_REG <= "01";
      WAIT UNTIL Clk_100MHz = HIGH;

    END LOOP; -- L_T02_01

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_02] FINISHED";
    WAIT FOR 1 us;
    REPORT "[TEST_03] INIT : Test DCC_CMD_ACTIVER_ANNONCE_ENTREE_EN_GARE Command";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    DCC_In  <= DCC_CMD_ACTIVER_ANNONCE_ENTREE_EN_GARE;
    COM_REG <= "00";
    WAIT UNTIL Clk_100MHz = HIGH;

    L_T03_01 : FOR i IN (DCC_NO_BITS - 1) DOWNTO 0 LOOP
      COM_REG <= "11";
      WAIT UNTIL Clk_100MHz = HIGH;
      ASSERT (DCC_bit_Out = DCC_CMD_ACTIVER_ANNONCE_ENTREE_EN_GARE(i)) REPORT "[TEST_03][Error]: DCC_bit_Out is not the bit expected" SEVERITY SEVERITY_LVL;
      IF NOT(DCC_bit_Out = DCC_CMD_ACTIVER_ANNONCE_ENTREE_EN_GARE(i)) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                                                              <= LOW;
      END IF;
      COM_REG <= "01";
      WAIT UNTIL Clk_100MHz = HIGH;

    END LOOP; -- L_T03_01

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_03] FINISHED";
    WAIT FOR 1 us;
    REPORT "[TEST_04] INIT : Test DCC_CMD_ETEINDRE_LES_PHARES Command";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    DCC_In  <= DCC_CMD_ETEINDRE_LES_PHARES;
    COM_REG <= "00";
    WAIT UNTIL Clk_100MHz = HIGH;

    L_T04_01 : FOR i IN (DCC_NO_BITS - 1) DOWNTO 0 LOOP
      COM_REG <= "11";
      WAIT UNTIL Clk_100MHz = HIGH;
      ASSERT (DCC_bit_Out = DCC_CMD_ETEINDRE_LES_PHARES(i)) REPORT "[TEST_04][Error]: DCC_bit_Out is not the bit expected" SEVERITY SEVERITY_LVL;
      IF NOT(DCC_bit_Out = DCC_CMD_ETEINDRE_LES_PHARES(i)) THEN ERROR_SIGNAL <= HIGH;
      ELSE ERROR_SIGNAL                                                              <= LOW;
      END IF;
      COM_REG <= "01";
      WAIT UNTIL Clk_100MHz = HIGH;

    END LOOP; -- L_T04_01

    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_04] FINISHED";

    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;