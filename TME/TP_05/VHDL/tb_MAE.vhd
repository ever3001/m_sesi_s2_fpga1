-- ----------------------------------------
-- VHDL Test Bench for the State Machine
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_MAE IS END;

ARCHITECTURE TEST OF TB_MAE IS
  --! ########   Signals for component    ########
  SIGNAL Clk_100MHz : STD_LOGIC := LOW;
  SIGNAL Reset      : STD_LOGIC := HIGH;

  --! ######## Registre

  --! ######## Tempo
  SIGNAL TMPO_StartFlag   : STD_LOGIC;        -- Starts to count
  SIGNAL TMPO_FinishFlag  : STD_LOGIC := LOW; -- End Signal
  SIGNAL TMPO_TimeValInMs : BIT_08    := x"06";

  --! ######## DCC_BIT_0
  SIGNAL DCC_BIT_0_GoFlag     : STD_LOGIC;        -- GoFlag signal
  SIGNAL DCC_BIT_0_FinishFlag : STD_LOGIC := LOW; -- End Signal
  --! ######## DCC_BIT_1
  SIGNAL DCC_BIT_1_GoFlag     : STD_LOGIC; -- GoFlag signal
  SIGNAL DCC_BIT_1_FinishFlag : STD_LOGIC := LOW;

  --! ####### REG               
  SIGNAL REG_COM_REG     : STD_LOGIC_VECTOR(1 DOWNTO 0); -- Command of register
  SIGNAL REG_END_REG     : STD_LOGIC;                    -- End of transmission
  SIGNAL REG_DCC_BIT_OUT : STD_LOGIC;                    -- transmited BIT

  SIGNAL STOP_SIM : STD_LOGIC := LOW; -- Flag to stop the simulation

BEGIN
  MAE_0 : ENTITY work.MAE(BEHAVIORAL)

    PORT MAP(
      Clk_100MHz           => Clk_100MHz,
      REG_END_REG          => REG_END_REG,
      REG_COM_REG          => REG_COM_REG,
      REG_DCC_BIT_OUT      => REG_DCC_BIT_OUT,
      TMPO_StartFlag       => TMPO_StartFlag,
      TMPO_FinishFlag      => TMPO_FinishFlag,
      TMPO_TimeValInMs     => TMPO_TimeValInMs,
      DCC_BIT_0_GoFlag     => DCC_BIT_0_GoFlag,
      DCC_BIT_0_FinishFlag => DCC_BIT_0_FinishFlag,
      DCC_BIT_1_GoFlag     => DCC_BIT_1_GoFlag,
      DCC_BIT_1_FinishFlag => DCC_BIT_1_FinishFlag,
      Reset                => Reset
    );
  Clk_100MHz <= NOT Clk_100MHz AFTER 5 ns WHEN STOP_SIM = LOW;

  TEST_PROC : PROCESS
  BEGIN

    Reset <= HIGH;
    WAIT UNTIL Clk_100MHz = LOW;

    -- Verification pour S0 (1) : Départ 
    Reset <= LOW;

    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= LOW AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "00" REPORT "ERROR : S0 " SEVERITY ERROR;

    -- Verification pour S1
    REG_END_REG <= LOW;
    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= LOW AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "10" REPORT "ERROR : S1 " SEVERITY ERROR;

    --Verification pour S2 (On ne fait pas S3 ici comme chemin)
    REG_DCC_BIT_OUT <= LOW;
    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= LOW AND DCC_BIT_0_GoFlag <= HIGH AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "01" REPORT "ERROR : S2 " SEVERITY ERROR;

    --Verification pour S4
    DCC_BIT_0_FinishFlag <= LOW;
    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= LOW AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "10" REPORT "ERROR : S4 " SEVERITY ERROR;

    --Verification pour S1 back (remontée dans la boucle)

    DCC_BIT_0_FinishFlag <= HIGH;-- On les 2 automatiquement à 1 par défaut 
    DCC_BIT_1_FinishFlag <= HIGH;

    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= LOW AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "10" REPORT "ERROR : S4 " SEVERITY ERROR;

    --Verification pour S5 

    REG_END_REG <= HIGH;
    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= HIGH AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "10" REPORT "ERROR : S5 " SEVERITY ERROR;

    --Verification pour S0 (2) : Arrivée 

    TMPO_FinishFlag <= HIGH;
    WAIT UNTIL Clk_100MHz = LOW;
    ASSERT TMPO_StartFlag <= HIGH AND DCC_BIT_0_GoFlag <= LOW AND DCC_BIT_1_GoFlag <= LOW AND REG_COM_REG <= "00" REPORT "ERROR : S0 (final) " SEVERITY ERROR;
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;