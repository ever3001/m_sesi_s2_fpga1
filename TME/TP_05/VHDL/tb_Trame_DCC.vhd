----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.05.2020 23:02:01
-- Design Name: 
-- Module Name: TB_Trame_DCC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_Trame_DCC IS
END TB_Trame_DCC;
ARCHITECTURE TEST OF TB_Trame_DCC IS

  SIGNAL Clk_100       : std_logic := '0';
  SIGNAL Reset         : std_logic;
  SIGNAL Trame_DCC_OUT : DCC_COMMAND;
  SIGNAL SWITCHS       : std_logic_vector(2 DOWNTO 0);
  SIGNAL Bouton_L      : std_logic;
  SIGNAL Bouton_R      : std_logic;
  SIGNAL Bouton_C      : std_logic;
  SIGNAL Led_addr      : std_logic_vector(2 DOWNTO 0);
  SIGNAL Led_cmd       : std_logic_vector(3 DOWNTO 0);

  SIGNAL STOP_SIM : STD_LOGIC := LOW; -- Flag to stop the simulation

BEGIN

  Generateur_de_trame : ENTITY work.Trame_DCC PORT MAP(
    Clk_100       => Clk_100,
    Reset         => Reset,
    Trame_DCC_OUT => Trame_DCC_OUT,
    SWITCHS       => SWITCHS,
    Bouton_L      => Bouton_L,
    Bouton_R      => Bouton_R,
    Bouton_C      => Bouton_C,
    Led_addr      => Led_addr,
    Led_cmd       => Led_cmd);

  Clk_100 <= NOT(Clk_100) AFTER 5 ns WHEN STOP_SIM = LOW;
  SWITCHS <= "100";--Train numéro 4

  PROCESS
  BEGIN

    Reset <= '1';
    WAIT UNTIL Clk_100 = '0';
    Reset    <= '0';
    Bouton_L <= '0';
    Bouton_R <= '0'; -- Vitesse du train plus lente
    Bouton_C <= '0';
    WAIT UNTIL Clk_100 = '0';
    Bouton_R <= '1';
    WAIT FOR 1 us;
    Bouton_R <= '0';
    WAIT FOR 1 us;
    Bouton_C <= '1';
    WAIT FOR 1 us;
    Bouton_C <= '0';
    WAIT FOR 1 us;
    ASSERT Trame_DCC_OUT = DCC_CMD_VITESSE_PLUS_RAPIDE_STEP_28 REPORT " ERROR TRAME" SEVERITY ERROR;
    STOP_SIM <= HIGH;
    WAIT;

  END PROCESS;
END test;