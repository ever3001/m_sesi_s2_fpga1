-- ----------------------------------------
-- VHDL Test Bench for DCC_BIT
-- ----------------------------------------
-- Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- March 2020

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;

ENTITY TB_DCC_BIT IS END;

ARCHITECTURE TEST OF TB_DCC_BIT IS

  --! ########   Signals for component    ########
  SIGNAL BIT_0_GoFlag     : STD_LOGIC := LOW;
  SIGNAL BIT_0_FinishFlag : STD_LOGIC;
  SIGNAL BIT_0_DccOut     : STD_LOGIC;

  SIGNAL BIT_1_GoFlag     : STD_LOGIC := LOW;
  SIGNAL BIT_1_FinishFlag : STD_LOGIC;
  SIGNAL BIT_1_DccOut     : STD_LOGIC;

  SIGNAL ClockIn          : STD_LOGIC := LOW;
  SIGNAL ClockOut         : STD_LOGIC;
  SIGNAL Reset            : STD_LOGIC := HIGH;

  SIGNAL STOP_SIM         : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST             : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL     : STD_LOGIC := LOW; -- Indicates if an assert is incorrect

BEGIN
  DCC_BIT_0 : ENTITY work.DCC_BIT(BEHAVIORAL)
    GENERIC MAP(BIT => 0)
    PORT MAP(
      Clk_100MHz => ClockIn,
      Clk_1MHz   => ClockOut,
      GoFlag     => BIT_0_GoFlag,
      FinishFlag => BIT_0_FinishFlag,
      DccOut     => BIT_0_DccOut,
      Reset      => Reset
    );

  DCC_BIT_1 : ENTITY work.DCC_BIT(BEHAVIORAL)
    GENERIC MAP(BIT => 1)
    PORT MAP(
      Clk_100MHz => ClockIn,
      Clk_1MHz   => ClockOut,
      GoFlag     => BIT_1_GoFlag,
      FinishFlag => BIT_1_FinishFlag,
      DccOut     => BIT_1_DccOut,
      Reset      => Reset
    );

  CLOCK_DIV_0 : ENTITY work.ClockDiv(BEHAVIORAL)
    GENERIC MAP(DIVIDING_FACTOR => CLOCK_DIVIDING_FACTOR)
    PORT MAP(
      ClockIn  => ClockIn,
      ClockOut => ClockOut,
      Reset    => Reset
    );

  ClockIn <= NOT(ClockIn) AFTER (CLOCK_PERIODE_IN_NS/2) WHEN STOP_SIM = LOW;

  PROCESS
  BEGIN
    Reset <= HIGH;
    WAIT UNTIL ClockIn = HIGH;
    Reset <= LOW;
    WAIT UNTIL ClockIn = HIGH;
    REPORT "[TEST_01] INIT : Test to save a value";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;
    -- ############

    -- ################## START TEST
    BIT_0_GoFlag <= HIGH;
    BIT_1_GoFlag <= HIGH;
    WAIT UNTIL BIT_1_FinishFlag = LOW AND BIT_0_FinishFlag = LOW;

    ASSERT (BIT_1_DccOut = LOW) REPORT "[TEST_01][Error]: BIT_1_DccOut is not correct after FinishFlag Low" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_1_DccOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    ASSERT (BIT_0_DccOut = LOW) REPORT "[TEST_01][Error]: BIT_0_DccOut is not correct after FinishFlag Low" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_0_DccOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    WAIT FOR 58 us;

    ASSERT (BIT_1_DccOut = HIGH) REPORT "[TEST_01][Error]: BIT_1_DccOut is not correct after 58 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_1_DccOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    ASSERT (BIT_0_DccOut = LOW) REPORT "[TEST_01][Error]: BIT_0_DccOut is not correct after 58 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_0_DccOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    WAIT FOR 42 us;

    ASSERT (BIT_1_DccOut = HIGH) REPORT "[TEST_01][Error]: BIT_1_DccOut is not correct after 100 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_1_DccOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    ASSERT (BIT_0_DccOut = HIGH) REPORT "[TEST_01][Error]: BIT_0_DccOut is not correct after 100 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_0_DccOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    WAIT FOR 16 us;

    ASSERT (BIT_1_DccOut = LOW) REPORT "[TEST_01][Error]: BIT_1_DccOut is not correct after 116 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_1_DccOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    ASSERT (BIT_0_DccOut = HIGH) REPORT "[TEST_01][Error]: BIT_0_DccOut is not correct after 116 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_0_DccOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    WAIT FOR 84 us;

    ASSERT (BIT_1_DccOut = HIGH) REPORT "[TEST_01][Error]: BIT_1_DccOut is not correct after 200 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_1_DccOut = HIGH) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    ASSERT (BIT_0_DccOut = LOW) REPORT "[TEST_01][Error]: BIT_0_DccOut is not correct after 200 us" SEVERITY SEVERITY_LVL;
    IF NOT(BIT_0_DccOut = LOW) THEN
      ERROR_SIGNAL <= HIGH;
    ELSE
      ERROR_SIGNAL <= LOW;
    END IF;

    WAIT UNTIL BIT_0_FinishFlag = HIGH;
    -- ################## END TEST

    -- Put the signals to default value
    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";
    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;
END ARCHITECTURE TEST;