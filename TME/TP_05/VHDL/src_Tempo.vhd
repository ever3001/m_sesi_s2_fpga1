-- Tempo.vhd
-- ---------------------------------------------
-- Counts for some time in ms
-- ---------------------------------------------
-- Version : 1.0
-- Date : March 2020
-- Author : Ever ATILANO, Joséphine MASINI, Apolline JOURET
-- ---------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY WORK;
USE WORK.DCC_COMMON_PACK.ALL;
-- ---------------------------------------------
    ENTITY Tempo IS
-- ---------------------------------------------
  PORT (
    Clk_1MHz    : IN STD_LOGIC;  -- Clock input 100 Mhz
    StartFlag   : IN STD_LOGIC;  -- Starts to count
    FinishFlag  : OUT STD_LOGIC; -- End Signal
    TimeValInMs : IN BIT_08;     -- InternalCounter value in micro seconds
    Reset       : IN STD_LOGIC   -- Async Reset
  );
END Tempo;

-- ---------------------------------------------
    ARCHITECTURE Behavioral OF Tempo IS
-- ---------------------------------------------
  --! ######## Signals for component ########
  SIGNAL InternalCounter           : BIT_08     := TEMPO_COUNTER_DEFAULT_VAL;
  SIGNAL FactorInternalCounter     : BIT_16     := (OTHERS => LOW);
  SIGNAL StartCounting             : STD_LOGIC  := LOW;
  SIGNAL CurrentState, FutureState : type_state := S0;

BEGIN
  PROCESS (Clk_1MHz, Reset)
  BEGIN
    IF (Reset = HIGH) THEN
      CurrentState <= S0;
    ELSIF rising_edge (Clk_1MHz) THEN
      CurrentState <= FutureState;
      IF StartCounting = HIGH THEN
        FactorInternalCounter <= FactorInternalCounter + 1;
        -- A 1 ms passed
        IF (FactorInternalCounter >= 1000 - 1) THEN
          -- Increment the internal counter by 1
          InternalCounter <= InternalCounter + 1;
          -- Reset Factor Internal Counter
          FactorInternalCounter <= (OTHERS => LOW);
        END IF;
      ELSE 
        InternalCounter  <= TEMPO_COUNTER_DEFAULT_VAL;
        FactorInternalCounter <= (OTHERS => LOW);
      END IF;
    END IF;
  END PROCESS;

  -- State Control;
  PROCESS (StartFlag, TimeValInMs, InternalCounter, FutureState)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 => FutureState <= S0;
        IF StartFlag = HIGH THEN
          FutureState <= S1;
        END IF;

      WHEN S1 => FutureState <= S1;
        IF (InternalCounter >= TimeValInMs) THEN
          FutureState <= S0;
        END IF;

      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;

  -- Output control
  PROCESS (CurrentState, StartCounting)
  BEGIN
    CASE (CurrentState) IS

      WHEN S0 =>
        StartCounting <= LOW;
        FinishFlag    <= HIGH;

      WHEN S1 =>
        StartCounting <= HIGH;
        FinishFlag    <= LOW;
      WHEN OTHERS => NULL;
    END CASE;
  END PROCESS;

END ARCHITECTURE Behavioral;