# FPGA Révision

## Table of contents

[[_TOC_]]

## VHDL

**V**ery High Speed Integrated Circuit  
**H**ardware  
**D**escription  
**L**anguage  

### Bloc de Base VHDL

!["Base VHDL"](./assets/img/VHDL_module.png)

#### Bibliothèque

Permet d'accéder à toutes les cellules ou fonctions élémentaires pour décrire le composant.  

```vhdl
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
```

#### Entité

- Vue externe du composant.
- Précise ses entrées et ses sorties.
- Définition de paramètres génériques.

```vhdl
-- ---------------------------------------------
    ENTITY ClockDiv IS
-- ---------------------------------------------
  PORT (
    ClockIn  : IN STD_LOGIC;  -- Clock input
    ClockOut : OUT STD_LOGIC; -- Clock output
    Reset    : IN STD_LOGIC   -- Async Reset
  );
END ClockDiv;
```

### Architecture

- Décrit le fonctionnement Interne du composant.
- Obligatoirement associée à une entité (1 entité peut être associée à plusieurs architectures).
- Différents niveaux de description possibles.

```vhdl
-- ---------------------------------------------
    ARCHITECTURE Behavioral OF ClockDiv IS
-- ---------------------------------------------

-- ########### CODE

END ARCHITECTURE Behavioral;
```

### Ports

Le port permet de rentrer ou de sortir une donnée dans le module que l'on décrit.  

```vhdl

PORT (
    Entrée          : TOTO: IN    std_logic;
    Sortie          : TITI: OUT   std_logic;
    -- A n'utiliser que si on veut VRAIMENT faire du bidirectionnel (description de bus, etc...)
    Bidirectionnel  : TUTU: INOUT std_logic;
);

```

### Signal

Un signal peut être vu comme un fil connectant deux parties d'un circuit

```vhdl
SIGNAL cmd : std_logic_vector(1 DOWNTO 0) := "00";
```

### Types de données

#### Types de Base

Tous ces types ne sont pas synthétisables (et servent juste à de la modélisation)

```vhdl
- Bit ('0', '1')
- Bit_vector
- Boolean (false, true)
- Integer (signal TOTO: integer range 0 to 7; )
- Natural (entiers >= 0)
- Positive (entiers > 0)
- Real
- Character
- String
- Time (fs, ps, ns, us, ms, sec, min, hr)
```

#### Types IEEE

La bibliothèque IEEE propose des types de données plus adaptés que les types de base (bit/bit_vector) à la représentation d'un signal numérique dans un circuit

```vhdl
- std_ulogic
- std_logic
- std_logic_vector (3 downto 0)
```

Le type **std_logic** est un type dérivé de **std_ulogic** mais s'appuie sur une fonction de résolution qui permet de gérer le cas où un signal/port dispose de plusieurs sources.

```vhdl
CONSTANT resolution_table : stdlogic_table := (
-- | 'U'  'X'  '0'  '1'  'Z'  'W'  'L'  'H'  '-' | |
   ( 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U' ), -- | 'U' |
   ( 'U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' ), -- | 'X' |
   ( 'U', 'X', '0', 'X', '0', '0', '0', '0', 'X' ), -- | '0' |
   ( 'U', 'X', 'X', '1', '1', '1', '1', '1', 'X' ), -- | '1' |
   ( 'U', 'X', '0', '1', 'Z', 'W', 'L', 'H', 'X' ), -- | 'Z' |
   ( 'U', 'X', '0', '1', 'W', 'W', 'W', 'W', 'X' ), -- | 'W' |
   ( 'U', 'X', '0', '1', 'L', 'W', 'L', 'W', 'X' ), -- | 'L' |
   ( 'U', 'X', '0', '1', 'H', 'W', 'W', 'H', 'X' ), -- | 'H' |
   ( 'U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' )  -- | '-' |
);
```

### Types Utilisateur

Ce sont les types crées par l'utilisateur par example :

```vhdl
-- Type tableau de données
type MY_TAB is array (0 to 7) of integer;
                      -- Tableau 1D de 8 entiers
-- Type tableau 2D de données (à l'aide du type std_logic_vector)
type MY_VEC_TAB is array (0 to 7) of std_logic_vector(3 downto 0);
                      -- Tableau 2D: 8 éléments de 4 bits
-- Type « Vrai » tableau 2D de données
type MY_2D_TAB is array (0 to 3, 0 to 5) of integer;
                      -- Tableau 2D de 4x6 entiers
-- Type Énuméré
type MY_TYPE is (GOOD,AVERAGE,BAD);
-- Sous-Type
subtype mini_integer is integer range 0 to 3;
```

### Opérateurs

#### Opérateurs logiques (bit à bit)

```vhdl
not
and
or
nand
nor
xor
xnor
&         (Concaténation)
```

### Opérateurs arithmétiques

```vhdl
+
-
*         (synthétisable mais quelle architecture?)
/         (peu synthétisable)
mod       (pas synthétisable)
exp       (pas synthétisable)
```

### Niveaux de description VHDL

- **Flot de Données :** Description bas-niveau. Écriture des équations logiques des sorties en fonction des entrées.  

```vhdl
ENTITY Add1b IS
  PORT (
    a, b, cin : IN std_logic;
    cout, sum : OUT std_logic);
END Add1b;
ARCHITECTURE Dataflow OF Add1b IS
BEGIN
  sum  <= a XOR b XOR cin;                         -- somme
  cout <= (a AND b) OR (b AND cin) OR (cin AND a); -- retenue
END Dataflow;
```

- **Structurel :** Utilisation de composants pré-définis et référencés dans la bibliothèque utilisateur.

```vhdl
ARCHITECTURE Struct OF Add4B IS
  SIGNAL c : std_logic_vector(3 DOWNTO 1);
BEGIN
  Adder_0 : ENTITY work.Add1B(Dataflow)
            PORT MAP(a(0), b(0), '0', sum(0), c(1));
  Adder_1 : ENTITY work.Add1B(Dataflow)
            PORT MAP(a(1), b(1), c(1), sum(1), c(2));
  Adder_2 : ENTITY work.Add1B(Dataflow)
            PORT MAP(a(2), b(2), c(2), sum(2), c(3));
  Adder_3 : ENTITY work.Add1B(Dataflow)
            PORT MAP(a(3), b(3), c(3), sum(3), sum(4));
END Struct;
```

- **Comportemental :** Description de plus haut niveau. Vision "algorithmique" ou fonctionnelle du système.  

```vhdl
ENTITY Add4b IS
  PORT (A, B : IN std_logic_vector(3 DOWNTO 0);
  Sum        : OUT std_logic_vector(4 DOWNTO 0);
END Add4b;
ARCHITECTURE Behavioral OF Add4B IS
BEGIN
  Sum <= '0' & A + B;
END Struct;
```

### Généricité

VHDL permet de paramétrer les tailles des données d'un composant. Avec l'interet de la réutilisation d'un même description pour plusieurs modules.

```vhdl
ENTITY Add_Nb IS
  GENERIC (N : NATURAL := 8);
  PORT (
    a, b : IN std_logic_vector(N - 1 DOWNTO 0);
    s    : OUT std_logic_vector(N - 1 DOWNTO 0));
END Add_Nb;

------------------------------------
ARCHITECTURE archi OF Top_Level IS
BEGIN
  L1 : ENTITY work.Add_Nb
    GENERIC MAP(4)
    PORT MAP(a1, b1, s1);

  L2 : ENTITY work.Add_Nb
    GENERIC MAP(32)
    PORT MAP(a2, b2, s2);
END arc;
```

### VHDL Concurrent

- Toutes les instructions d'une architecture VHDL sont concurrentes et sont traitées en parallèle par le simulateur.  

- **when else**

```vhdl
Q <= '0' WHEN Raz <= '0'
  ELSE D WHEN rising_edge(H)
  -- else Q
  ;
```

- **with select**

```vhdl
WITH Com SELECT
  S <= A WHEN "00",
  B WHEN "01",
  C WHEN "10",
  D WHEN "11";
```

- **generate**

### VHDL Séquentiel

- Il existe cependant une structure de programmation appelée process, à l'intérieur de laquelle les instructions sont traitées de manière séquentielle.

#### Process

Un **process** peut être vu comme une fonction dont l'exécution dépend de l'apparition d'événements (Variation de signaux/ports, délai...). Il permet une description plus algorithmique du système.Les événements que l'on souhaite observer font partie de la **liste de sensibilité** du process.

- **Liste de sensibilité :**  la liste de sensibilité d'un process ne doit contenir que des signaux/ports à observer (si la liste comprend un délai, le process n'est pas synthétisable).
  - Si un des événements de la liste survient, le process s'exécute.
  - Si aucun événement ne survient, le process reste en veille.

```vhdl
ARCHITECTURE archi OF Bascule IS
BEGIN
  PROCESS (h, raz) -- Liste de sensibilité
  BEGIN
    -- ##### CODE DU PROCESS
  END PROCESS;
END archi;
```

#### Affectations Conditionnelles

- **IF-THEN-ELSE-END IF**

```vhdl
PROCESS (h, raz)
BEGIN
  IF raz = '0' THEN
    q <= '0';
  ELSIF rising_edge(h) THEN
    q <= d;
  END IF;
END PROCESS;
```

- **CASE-WHEN**

```vhdl
PROCESS (a, b, c, d, com)
BEGIN
  CASE (com) IS
    WHEN "00"   => s <= a;
    WHEN "01"   => s <= b;
    WHEN "10"   => s <= c;
    WHEN "11"   => s <= d;
    WHEN OTHERS => NULL;  -- Pour couvrir le autres cas possibles...
  END CASE;
END PROCESS;
```

- **FOR-LOOP**

```vhdl
PROCESS (a, b)
BEGIN
  FOR i IN 0 TO 3 LOOP
    s(i) <= a(3 - i) AND b(i);
  END LOOP;
END PROCESS;
```

#### Instruction WAIT

Elle permet de mettre en veille l'exécution d'un process. **WAIT est une expression non synthétisable**, on l'utilise pour la simulation et les test bench.

- **WAIT ON :** Permet de réveiller le process dès qu'un des éléments de la liste change de valeur.

```vhdl
PROCESS
BEGIN
  WAIT ON (a, b)
  s <= a AND b;
END PROCESS;
--- C'est égal à écrire
PROCESS (a, b)
BEGIN
  s <= a AND b;
END PROCESS;
```

- **WAIT UNTIL :** Permet de réveiller le process dès que la condition est vérifiée

```vhdl
PROCESS
BEGIN
  WAIT UNTIL rising_edge(clk)
  Q <= D;
END PROCESS;
```

- **WAIT FOR :** permet de réveiller le process après un certain temps

```vhdl
PROCESS (a, b)
BEGIN
  FOR i IN 0 TO 3 LOOP
    s(i) <= a(3 - i) AND b(i);
  END LOOP;
END PROCESS;
```

## Système sur puce programmable (SoPC)
<!---TODO: -->

### BUS AXI

Le standard AXI4 regroupe 3 types de bus.

1. AXI4 (**Full**): Pour des systèmes de type memory-mapped.  
2. AXI4-**Lite**: Version simplifiée du précédent. Pour des applications avec un faible volume d’échanges de données.  
3. AXI4-**Stream**: Bus direct entre le µP et un périphérique
Pour des applications gérant un volume important de données

#### AXI4-Lite

Le bus est organisé en 5 canaux/channels :  

- Transferts µP -> Périphérique (Ecriture)
  - **Write Address Channel.**
  - **Write Data Channel.**  
  - **Write Response Channel.**  
- Transferts Périphérique -> µP (Lecture).  
  - **Read Address Channel.**  
  - **Read Data Channel.**  

Contrôle de type handshake  

- Un transfert a lieu uniquement si émetteur et récepteur sont OK
- Signaux VALID/READY propres à chaque canal

##### AXI4-Lite Écriture

!["AXI Lite Écriture"](./assets/img/AXI_LITE_ECRITURE.jpg "AXI Lite Écriture")

##### AXI4-Lite Lecture

!["AXI Lite Lecture"](./assets/img/AXI_LITE_LECTURE.jpg "AXI Lite Lecture")

### Flot de conception

!["Flot de conception"](./assets/img/FLOT_CONCEPTION.jpg "Flot de conception")

1. **Sélection** et configuration des composants standard de la plate-forme (µP, GPIO, Timers…).  
2. **Description** de l’architecture interne des fonctions spécifiques que l’on souhaite adjoindre au Microblaze.  
3. **Simulation** fonctionnel de chaque fonction spécifique Debug…  
4. **Génération** automatique de l’interface AXI pour chaque fonction spécifique. **Connexion** du périphérique custom au bus AXI. **Mise à jour** automatique du plan d’adressage du système.  
5. **Génération** du code de niveau top-level . **Instanciation** des IP standards et custom.  
6. **Identification** des primitives de base composant chaque module de la plate-forme. **Génération** d’une Netlist listant toutes les primitives. **Vérification** des contraintes de placement (I/O). **En cas d’erreur**, il faut revoir la structure de la plate-forme et/ou le code HDL des périphériques custom.  
7. **Placement** des primitives de base sur les cellules disponibles du FPGA. **Routage** des cellules en fonction des ressources disponibles sur le FPGA. Ces étapes s’effectuent par rapport à d’éventuelles contraintes de timing.  
8. **Création** du fichier de configuration du FPGA à partir des résultats de l’implémentation.  

**FIN DU DÉVELOPPEMENT MATERIEL**  

9. **Génération** d’un fichier résumant les principales 
caractéristiques de la plate-forme matérielle (type de 
composants, plan d’adressage).  
10. **BSP**: Ensemble des drivers et déclarations permettant/facilitant la gestion de la plate-forme matérielle par le logiciel. **Ajout** des drivers des périphériques standard et custom aux bibliothèques accessibles au code de l’application.  
11. **Développement** (C ou C++) de l’application (s’appuyant sur les ressources offertes par le BSP).  
12. **Compilation**. **Editions** de liens. **Génération** binaire exécutable. **Retour au code** source en cas d’erreurs…  
13. **Téléchargement** du bitstream Application SW dans le FPGA
**Téléchargement** du binaire exécutable dans la mémoire du processeur Microblaze.
14. **Debug** de l’application. **Selon les erreurs**, il faut **corriger** l’application logicielle ET/OU la plate-forme 
matérielle Debug.  


## Machines à États et Conception d'Intellectual Property (IP)

### Intellectual Property (IP)

- Module logique virtuel destiné à être ajouté comme partie d'un système.  
  - IP soft-core: Description HDL ou netlist.  
  - IP hard-core: Layout.  

- **Conception de SoC**.
  - Assemblage de modules IP.
  - Développés en interne.
  - Achetés à l’extérieur.
  - "Glue" pour faire communiquer ces IP ensemble.
  - Exemple: Bus AXI.

- **La qualité** d’une IP va être mesurée.
  - Par ses **performances**.
  - Par sa **fiabilité**.
    - Simulations, Testbenchs, Vérification.
  - Par sa **réutilisabilité**.
    - Paramètres de configuration.
    - Adapté à plusieurs plates-formes.
    - Documentation de l’IP.

#### Structuration IP

- Calcul et Mémorisation (Partie opérative- PO)
- Contrôle (Partie contrôle - PC) : Le rôle de la partie contrôle va être de mettre en œuvre les états successifs du système implémenté sous la forme d’une machine à états (finis).

!["Structuration IP"](./assets/img/STRUCTURATION_IP.jpg "Structuration IP")

### Machines à Etats

!["MAE Exemple"](./assets/img/MAE_EXEMPLE.jpg "MAE Exemple")

#### Machines de Moore - VHDL

Description à l'aide de process

1. Process décrivant le séquentiel (registre d'états).  
2. Process décrivant le combinatoire des états futurs.  
3. Process décrivant le combinatoire des sorties.  

```vhdl
ENTITY MAE IS
  PORT (
    h, raz : IN std_logic;
    e1, e2 : IN std_logic;
    X, Y   : OUT std_logic);
END MAE;

ARCHITECTURE Moore3 OF MAE IS
  -- Definition d'un type etat
  TYPE etat IS(S0, S1, S2, S3);
  SIGNAL EP, EF : etat;
BEGIN
  -- Process du Registre d'etats
  PROCESS (h, raz)
  BEGIN
    IF raz = '0' THEN
      EP <= S0;
    ELSIF rising_edge(h) THEN
      EP <= EF;
    END IF;
  END PROCESS;

  -- Combinatoire des etats
  PROCESS (EP, e1, e2)
  BEGIN
    CASE (EP) IS
      WHEN S0 => EF <= S3; IF e1 = '1' THEN EF <= S1; END IF;
      WHEN S1 => EF <= S2;
      WHEN S2 => EF <= S2; IF e2 = '1' THEN EF <= S3; END IF;
      WHEN S3 => EF <= s0;
    END CASE;
  END PROCESS;

  -- Combinatoire des sorties
  PROCESS (EP)
  BEGIN
    CASE (EP) IS
      WHEN S0 => X <= '1'; Y <= '0';
      WHEN S1 => X <= '0'; Y <= '1';
      WHEN S2 => X <= '1'; Y <= '0';
      WHEN S3 => X <= '1'; Y <= '1';
    END CASE;
  END PROCESS;

END Moore3;
```

## Mémoires

La mémoire est organisée en mots (**word**). Selon le composant 1 **word** = 8, 16, 32, 64, (…) bits. 1 mot est rangé dans une case de la mémoire.

!["Classification mémoires"](./assets/img/CLASSIFICATION_MEMOIRES.png "Classification mémoires")

Historiquement, les mémoires étaient classées selon les termes.  

- **Volatile :** La mémoire perd ses données si elle n'est pas alimentée.  
- **Non Volatile :** La mémoire garde ses données, même sans alimentation.  

**NB :**

- Les ROM sont aussi des mémoires à accès aléatoire.  
- On peut écrire des données dans des ROM.  
- Cas des mémoires Flash (clés USB).  

---

### Classification Mémoires

- [RAM: Random Access Memory](#m%C3%A9moires-ram-random-access-memory)
  - [SRAM: Static RAM](#static-ram-sram)
  - [DRAM: Dynamic RAM](#dynamic-ram-dram)
- NVRAM: Non Volatile RAM
  - [MRAM: Magnetic RAM](#m%C3%A9moires-mram-magnetic-ram)
  - FeRAM: Ferroelectric RAM
- [ROM: Read Only Memory](#m%C3%A9moires-rom-read-only-memory)
  - [MROM: Mask ROM](#mask-rom-mrom)
  - [PROM: Programmable ROM](#programmable-rom-prom)
  - [EPROM: Erasable PROM](#erasable-programmable-rom-eprom)
  - [UV-EPROM: Ultra-Violets Erasable PROM](#uv-eprom)
  - [EEPROM: Electrically Erasable PROM](#eeprom)

#### Mémoires RAM (Random Access Memory)
<!---TODO: -->
##### Static RAM (SRAM)
<!---TODO: -->
##### Dynamic RAM (DRAM)
<!---TODO: -->
#### Mémoires ROM (Read Only Memory)
<!---TODO: -->
##### Mask ROM (MROM)
<!---TODO: -->
##### Programmable ROM (PROM)
<!---TODO: -->
##### Erasable Programmable ROM (EPROM)
<!---TODO: -->
###### UV-EPROM
<!---TODO: -->
###### EEPROM
<!---TODO: -->
#### Mémoires FLASH
<!---TODO: -->
##### NOR-Flash
<!---TODO: -->
##### NAND-Flash
<!---TODO: -->
#### Mémoires MRAM (Magnetic RAM)
<!---TODO: -->
## JTAG

### Boundary Scan

#### Entrées/Sorties

!["Input/Output"](./assets/img/E_S_JTAG.png "Input/Output")

##### Example avec 4 circuits

!["Example avec 4 circuits boundary scan"](./assets/img/CIRCUIT_BOUNDARY_SCAN.png "Example avec 4 circuits boundary scan")

### Cellule Boundary Scan

!["Cellule Boundary Scan"](./assets/img/CELLULE_BS.jpg "Cellule Boundary Scan")

Il peut avoir 4 modes de fonctionnement :  

1. **Test-Logic-Reset** (mode: transparent) : Les fonctionnalités de test sont désactivées
2. **Capture** :  Recopie des sorties d'un circuit. Observation des sorties d'un circuit suite à un test.
3. **Update** : Les entrées fonctionnelles du circuit sont court-circuitées. Insertion de stimulis en entrée du circuit via le scan-path.
4. **Serial Shift** : E/S de données de test depuis/vers l'extérieur de la carte. Décalage des données dans le scan-path

### Registres Boundary scan

#### Registre Instruction

#### Registres de données

#### Tap Controller

!["Tap Controller"](./assets/img/JTAG_TAP_Controller_State_Diagram.svg "Tap Controller")

##### Description des états

- **Select-DR/IR**
  - Sélection du mode instruction ou données
- **Capture-DR/IR**
  - Chargement parallèle de la partie "shift register" du registre de données/instruction sur front montant de TCK
- **Shift-DR/IR**
  - Décalage des données de TDI vers TDO dans le registre de données/instruction sur front montant de TCK
- **Exit1/2-DR/IR**
  - Etats temporaires
- **Pause-DR/IR**
  - Etat d'attente de valeurs sur TDI
- **Update-DR/IR**
  - Transfert du "shift register" (instructions/données) vers le "parallel register" sur front descendant de TCK

#### Liste des Instructions

| Nom Instruction | Obligatoire |   Code  |                                                      Rôle                                                     |
|:---------------:|:-----------:|:-------:|:-------------------------------------------------------------------------------------------------------------:|
|      **BYPASS**     |     Oui     |  11...1 | Permet de "court-circuiter" le BSR d'un circuit en le remplaçant par un Bypass Registre d'un bit.             |
|      **EXTEST**     |     Oui     |  00...0 | Déconnection des plots d'E/S du circuit. Permet le test des interconnections entre deux circuits grâce au BSR |
|  **SAMPLE/PRELOAD** |     Oui     | A fixer | Test du circuit en fonctionnement normal grâce au BSR                                                         |
|      **INTEST**     |     Non     | A fixer | Test du circuit grâce au BSR par insertion de valeurs de test                                                 |
|      **IDCODE**     |     Non     | A fixer | Chargement du contenu d'un DeviceIdentification Register dans le Registre Instruction                         |
|     **RUNBIST**     |     Non     | A fixer | Exécution d'un pattern de test et sauvegarde grâce à des RunBIST Register.                                    |
|    **Autres...**    |     Non     | A fixer | Configuration FPGA...                                                                                         |

##### Instruction BYPASS

Permet de raccourcir la longueur du scan-path pour des circuits que l'on ne souhaite pas tester.

##### Instruction SAMPLE/PRELOAD

- **Fonction SAMPLE :** Permet de faire une capture de l'état des E/S d'un circuit puis de les faire sortir via le scan-path du BSR, sans interférer avec le comportement du circuit.  

- **Fonction PRELOAD :** Permet de charger une valeur dans les cellules du scan-path pour préparer un test (avec l'instruction EXTEST).  

##### Instruction EXTEST

- Test des interconnexions entre deux circuits ($`IC_1`$ -> $`IC_2`$)
- La logique interne est « isolée » des E/S
- **L'instruction est exécutée après un SAMPLE/PRELOAD** qui a positionné des valeurs de test en sortie du $`IC_1`$

##### Instruction INTEST

- Instruction optionnelle, similaire à EXTEST, mais pour le test de la logique interne d'un circuit.  
- **L'instruction est exécutée après un SAMPLE/PRELOAD** qui a positionné des valeurs de test en entrée du circuit à tester

##### Exemple: Chargement d'une instruction 3 bits: I$`_2`$ I$`_1`$ I$`_0`$

!["Exemple chargement instruction"](./assets/img/EXEMPLE_CHARGEMENT_INSTRUCTION.jpg "Exemple chargement instruction")
