LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
--USE IEEE.std_logic_unsigned.ALL;

ENTITY compt_decompt IS
  PORT (
    clk, reset, UD : IN std_logic;
    q              : OUT std_logic_vector(1 DOWNTO 0)
  );
END ENTITY compt_decompt;

ARCHITECTURE comport OF compt_decompt IS
  SIGNAL qint : unsigned(1 DOWNTO 0);
BEGIN
  UpDown : PROCESS (clk, reset, UD) IS
  BEGIN
    IF (reset = '0') THEN
      qint <= "00";
    ELSIF (clk'event AND clk = '1') THEN
      IF (UD = '0') THEN
        qint      <= qint + 1;
      ELSE qint <= qint - 1;
      END IF;
    ELSE qint <= qint;
    END IF;
  END PROCESS UpDOwn;

  q <= std_logic_vector(qint);
END ARCHITECTURE comport;