LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_compt_decompt IS END;

ARCHITECTURE TEST OF tb_compt_decompt IS
  CONSTANT CLOCK_PERIODE_100MHZ_IN_NS : TIME      := 10 ns; -- 1 ns = 100 MHz
  CONSTANT HIGH                       : STD_LOGIC := '1';
  CONSTANT LOW                        : STD_LOGIC := '0';

  --! ########   Signals for component    ########
  SIGNAL clk, UD : std_logic := LOW;
  SIGNAL reset   : std_logic := HIGH;
  SIGNAL q       : std_logic_vector(1 DOWNTO 0);

  SIGNAL STOP_SIM     : STD_LOGIC := LOW; -- Flag to stop the simulation
  SIGNAL TEST         : STD_LOGIC := LOW; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := LOW; -- Indicates if an assert is incorrect
BEGIN
  COMPT_DECOMPT_0 : ENTITY work.compt_decompt(comport)
    PORT MAP(
      clk   => clk,
      reset => reset,
      UD    => UD,
      q     => q
    );

  clk <= NOT(clk) AFTER (CLOCK_PERIODE_100MHZ_IN_NS/2) WHEN STOP_SIM = LOW;

  PROCESS
  BEGIN
    -- ###################### RESET OF THE SYSTEM
    Reset <= LOW;
    WAIT FOR 15 ns;
    -- ##########################################

    REPORT "[TEST_01] INIT";
    -- ############ Signals initialization
    TEST         <= HIGH;
    ERROR_SIGNAL <= LOW;

    Reset <= HIGH;
    UD    <= LOW;
    WAIT UNTIL clk = LOW;
    -- Increment variable
    L_T01_01 : FOR i IN 1 TO 10 LOOP
      WAIT UNTIL clk = HIGH;
    END LOOP; -- L_T01_01

    Reset <= HIGH;
    UD    <= HIGH;
    WAIT UNTIL clk = HIGH;
    -- Decrement variable
    L_T01_02 : FOR i IN 1 TO 8 LOOP
      WAIT UNTIL clk = HIGH;
    END LOOP; -- L_T01_02

    TEST <= LOW;
    REPORT "[TEST_01] FINISHED";

    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= HIGH;
    WAIT;
  END PROCESS;

  CHECK_Q : PROCESS (clk, Reset)
  BEGIN
    IF (Reset = '0') THEN ASSERT (q = "00") REPORT "Sortie n'est pas '00' dans le reset " & TIME'image(now) SEVERITY ERROR;
    END IF;
  END PROCESS CHECK_Q;
END ARCHITECTURE TEST;